
from manimlib import *

class CircleFraction(VGroup):
    def __init__(
                self,
                partitions,
                radius=0.7,
                fill_opacity=1,
                color=GREEN,
                add_stroke=False,
                stroke_kwargs={"stroke_width": 0.5, "stroke_color": BLACK},
                **kwargs):
        digest_config(self, kwargs)
        super().__init__(**kwargs)
        angle = 2 * PI / partitions
        self.angle = angle
        self.partitions = partitions
        self.radius = radius
        parts = self.get_parts()
        self.parts = parts
        if partitions < 4:
            slices = self.get_slices(parts,fill_opacity=fill_opacity, color=color, **kwargs)
        else:
            slices = self.get_slices_(parts,fill_opacity=fill_opacity, color=color, **kwargs)
        self.add(*slices)
        self.rotate(PI/2 - angle)
        if add_stroke:
            strokes = self.get_stoke_slice(parts, **stroke_kwargs)
            strokes.rotate(PI/2 - angle)
            self.strokes = strokes
            self.add(strokes)
        
    def get_parts(self):
        a, p = self.angle, self.partitions
        return VGroup(*[
            Arc(start_angle=a*i, angle=a, radius=self.radius)
            for i in range(p)
        ])
        
    def get_slices(self, parts, **kwargs):
        slices = VGroup()
        for p in parts:
            part = VMobject(**kwargs)
            center = p.get_arc_center()[0]
            part.append_vectorized_mobject(Line(center,p.get_start()))
            part.append_vectorized_mobject(p)
            part.append_vectorized_mobject(Line(p.get_end(),center))
            slices.add(part)
        return slices

    def get_slices_(self, parts, **kwargs):
        slices = VGroup()
        for p in self.parts:
            part = VMobject(**kwargs)
            part.append_points(Line(ORIGIN,p.get_start()).get_points())
            part.append_points(p.get_points())
            part.append_points(Line(p.get_end(),ORIGIN).get_points())
            slices.add(part)
        return slices
    
    def get_stoke_slice(self, parts, **kwargs):
        slices = VGroup()
        for p in parts:
            part = VMobject(**kwargs)
            part.add(Line(ORIGIN,p.get_start(),**kwargs))
            part.add(p.set_style(**kwargs))
            part.add(Line(p.get_end(),ORIGIN,**kwargs))
            slices.add(part)
        return slices
    
    def separe_slices(self, buff=0.2):
        self.slices_joined = self.deepcopy()
        center = self.get_center()
        vectors = [s.get_center()-center for s in self]
        unit_vectors = [normalize(v) for v in vectors]
        [s.shift(uv * buff) for s,uv in zip(self, unit_vectors)]
        # print(vectors)


class JoinSlices(MoveToTarget):
    def __init__(self, cake: CircleFraction, **kwargs):
        cake.generate_target()
        cake.target.become(cake.slices_joined)
        cake.target.move_to(cake)
        super().__init__(cake, **kwargs)


def return_sub_scene(cls: Scene, method_name, ret=True, mob=Rectangle(2,1), args=[]):
    method = getattr(cls, method_name)
    if ret:
        print(f"==> Start {method_name}")
        r = method(*args)
        print(f"==> End {method_name}")
        if r == None:
            print(f"==> You forgot return some mob in {method_name}")
            return mob
        else:
            return r
    else:
        return mob

def get_rect_config_from_all_mobs(mob: Mobject):
    w = mob.get_width()
    h = mob.get_height()
    x,y,z = mob.get_center()
    print(f"Rectangle({w},{h}).move_to([{x},{y},{z}])")

def get_positition_to_tex(slice, proportion, buff=0):
    center = slice[0].get_center()
    arc_center = slice[1].point_from_proportion(0.5)
    line = Line(center, arc_center)
    uv = line.get_unit_vector()
    reference_line = Line(center, arc_center + uv*buff)
    return reference_line.point_from_proportion(proportion)

# TEMPORARY MOBJS
R1 = Rectangle(2.9999999999999996,2.336293702853317)\
    .move_to([-4.41111111111111,1.6318531485733414,0.0])


def get_frac_string(n,d):
    return ["{",*n,"\\over",*d,"}"]

def get_frac_color(n,d,cn,cd,**kwargs):
    tex = Tex(*get_frac_string(n,d),**kwargs)
    tex[0].set_color(cn)
    tex[-1].set_color(cd)
    return tex
class MainSceneFracs(Scene):
    CONFIG = {
        "SCALE_TEX": 1.4
    }
    def pause(self, t=1):
        self.wait(t)

    def construct(self):
        # First 3 columns
        self.add_sound("music1")
        self.wait(14)
        r1 = return_sub_scene(self, "sub_scene_1", True, R1)
        self.wait(2)
        r2 = return_sub_scene(self, "sub_scene_2", True, R1, [r1])
        self.wait(2)
        r3 = return_sub_scene(self, "sub_scene_3", True, R1, [VGroup(r1,r2)])
        self.wait(4)
        thumb1 = return_sub_scene(self, "make_thumb_1", True, R1, [VGroup(r1,r2,r3)])
        self.wait(2)
        # Second 3 columns
        r4 = return_sub_scene(self, "sub_scene_4", True, R1)
        r5 = return_sub_scene(self, "sub_scene_5", True, R1, [r4])
        self.wait(1)
        r6 = return_sub_scene(self, "sub_scene_6", True, R1, [Mobject()])
        self.wait(5)
        thumb2 = return_sub_scene(self, "make_thumb_2", True, R1, [VGroup(r4,r5,r6)])
        self.wait(3)
        self.add_sound("music2",gain=-10) # decrease 10 % volume
        self.wait(19)
        # # Sum
        r7 = return_sub_scene(self, "sub_scene_7", True, R1, [Mobject()]) # Sum cake and sum example
        self.wait(3)
        # Example
        ss = return_sub_scene(self, "shrink_sum", True, R1, [r7]) # Shift sum to top
        self.wait(3)
        # Sum fraction example
        r8 = return_sub_scene(self, "sub_scene_8", True, R1, [ss]) # Adition
        # subtraction example
        r9 = return_sub_scene(self, "sub_scene_8", True, R1, [ss,[[4,9],[2,11]],-1,[RED,BLUE,PURPLE],RIGHT,lambda x: x.to_edge(RIGHT), False])
        self.wait(4)
        # make thumbs
        self.final_screen(thumb1, thumb2, ss, r8, r9)
        
    def final_screen(self, t1, t2, t3, e1, e2):
        r1 = VGroup(t1,t2)
        e = VGroup(e1,e2)
        r2 = VGroup(t3,e)
        r1.generate_target()
        r2.generate_target()
        r1t = r1.target
        r2t = r2.target
        t1t,t2t = r1t
        t3t,et = r2t
        # ------
        r1t.scale(1.2)
        r1t.arrange(RIGHT,buff=0.8)\
            .to_edge(UP)
        r2t[0].set_width(r1t[0].get_width())\
            .next_to(r1t[0],DOWN,0.5,LEFT)
        et = r2t[1]
        et.arrange(RIGHT,buff=1.3)\
            .set_width(r1t[1].get_width())
        et.next_to(r1t[1],DOWN,0.5,LEFT)
        et.align_to(r2t[0],DOWN)
        t = VGroup(r1t,r2t)
        t.set_width(FRAME_WIDTH-0.5)
        t.set_y(0)
        #
        r1t[0][:-1].set_width(r1t[0][-1].get_width())
        r1t[1][:-1].set_width(r1t[1][-1].get_width())
        #
        r1t[0][-1].scale(1.1)
        r1t[1][-1].scale(1.1)
        r1t[0][-1].scale([0.95,1,0])
        r1t[1][-1].scale([0.95,1,0])
        r1t[1][-1].set_height(r1t[0][-1].get_height(),stretch=True)
        f4 = Rectangle()\
            .set_width(t1t.get_width(),stretch=True)\
            .set_height(t3t.get_height(),stretch=True)\
            .set_x(t1t.get_x())\
            .set_y(t3t.get_y())\
            .scale([1,1.1,1])
        f5 = f4.deepcopy()
        f5.set_x(r1t[1][-1].get_x())
        f5.set_width(r1t[1][-1].get_width(),stretch=True)
        # Animation
        self.play(
            MoveToTarget(r1),
            MoveToTarget(r2),
            AnimationGroup(
                Animation(Mobject(),run_time=1.5),
                AnimationGroup(ShowCreation(f4),ShowCreation(f5)),
                lag_ratio=1
            ),
            run_time=3
        )
        self.wait()

    def sub_scene_1(self):
        # --- SETUP
        SCALE_TEX = self.SCALE_TEX
        cake_3 = CircleFraction(3)
        cake_3.separe_slices()
        slice = cake_3[0].deepcopy()
        count = VGroup(*[
            Tex(f"{n}\\times")
                .scale(SCALE_TEX) 
            for n in range(1,4)
        ])
        count.next_to(cake_3,LEFT,0.4)
        count[-1][0][0].set_color(RED)
        equal_sign = Tex("=")\
            .scale(SCALE_TEX)
        arrange_grp = VGroup(count,slice,equal_sign,cake_3)
        arrange_grp.arrange(RIGHT)
        # self.add(arrange_grp)
        # --- i = 1
        self.play(Write(count[0]))
        self.play(DrawBorderThenFill(slice))
        self.play(Write(equal_sign))
        self.play(DrawBorderThenFill(cake_3[0]))
        # i = 2
        self.play(ReplacementTransform(count[0],count[1]))
        self.play(DrawBorderThenFill(cake_3[1]))
        # i = 3
        self.play(ReplacementTransform(count[1],count[2]))
        self.play(DrawBorderThenFill(cake_3[2]))
        self.wait()
        # Join slices
        self.add(cake_3)
        cake_3.slices_joined.set_color(BLUE)
        self.play(JoinSlices(cake_3))
        # -------------------------------------------
        # -------------------------------------------
        # Setup second row
        _3x_tex = count[-1].deepcopy()
        _1_over_3 = Tex("\\frac{1}{3}")\
            .scale(SCALE_TEX)\
            .set_color(slice.get_color())
        _equal_sign = equal_sign.deepcopy()
        _1 = Tex("1")\
            .scale(SCALE_TEX)\
            .set_color(cake_3.get_color())
        _arrange_grp = VGroup(_3x_tex, _1_over_3, _equal_sign, _1)
        # Arrange second row
        _arrange_grp\
            .arrange(RIGHT)\
            .next_to(arrange_grp,DOWN,0.5)
        for i, _i in zip(arrange_grp, _arrange_grp):
            _i.set_x(i.get_x())
        # self.add(_arrange_grp)
        # Animations
        self.play(TransformFromCopy(count[-1],_3x_tex))
        self.play(FadeTransform(slice.deepcopy(), _1_over_3))
        self.play(TransformFromCopy(equal_sign,_equal_sign))
        self.play(FadeTransform(cake_3.deepcopy(), _1))
        all_mobs = VGroup(*self.mobjects)
        all_mobs.generate_target()
        amt = all_mobs.target
        amt.set_width(3)
        amt.to_corner(UL,buff=1.2)
        amt.shift(LEFT*0.4)
        self.wait()
        self.play(MoveToTarget(all_mobs))
        # get_rect_config_from_all_mobs(all_mobs)
        return all_mobs
    
    def sub_scene_2(self, previus_mobs: VGroup):
        SCALE_TEX = self.SCALE_TEX
        STATIC_COLOR = BLUE
        SILCE_INDEX = 0
        # First line
        n1 = Tex("1",color=STATIC_COLOR)\
            .scale(SCALE_TEX)
        equal_sign = Tex("=")\
            .scale(SCALE_TEX)
        cake = CircleFraction(3,color=BLUE)
        # cake.rotate(PI)
        right_line = Line(UP,DOWN,color=RED)\
            .set_height(cake.get_height())
        dd3 = Tex(":","3")\
            .scale(SCALE_TEX)\
            .arrange(RIGHT,buff=0.2)
        dd3[-1].set_color(RED)
        dd3__ = dd3.deepcopy()\
            .set_fill(opacity=0)
        L1 = VGroup(n1,dd3__,equal_sign,cake,right_line,dd3)\
            .arrange(RIGHT,buff=0.2)\
            .set_height(max(*[m.get_height() for m in previus_mobs]))\
            .align_to(previus_mobs,UP)\
            # .remove(dd3__)
        L1.shift(LEFT*0.25) # <-------------- ALIGN
        L1[-3:].shift(RIGHT*0.3)
        L1[-2:]\
            .shift(RIGHT*0.4)\
            #.set_color(RED)
        # Second line
        slice = cake[SILCE_INDEX].deepcopy()
        slice.set_color(GREEN)
        dd3_ = dd3__.deepcopy()
        dd3_[-1].set_fill(RED,1)
        dd3_[0].set_fill(WHITE,1)
        L2 = VGroup(
                n1.deepcopy(),
                dd3_,
                equal_sign.deepcopy(),
                slice
            )\
            .arrange(RIGHT,buff=0.1)\
            .set_y(previus_mobs[-1].get_y())
        for i in range(len(L2)):
            L2[i].set_x(L1[i].get_x())
        # Third line
        L3 = L2[:3].deepcopy()
        frac = previus_mobs[-3].deepcopy()
        frac.move_to(L2[-1])
        L3.add(frac)
        line_distance = L2.get_center() - L1.get_center()
        L3.move_to(L2.get_center()+line_distance)\
            .align_to(L2,LEFT)
        # -----------------------------
        L1.remove(dd3__)
        temp_circle = Circle(
                radius=cake.get_width()/2,
                fill_opacity=1
            )\
            .move_to(cake)\
            .set_color(BLUE)
        cake.generate_target()
        ct = cake.target
        ct.separe_slices()
        # RECOLOR
        color_func = lambda x: {"fill_color": x, "stroke_color": x}
        dd3[0][0].set_style(**color_func(WHITE)) # :3, : color white
        dd3_[0][0].set_style(**color_func(WHITE)) # :3, : color white
        dd3__[0][0].set_style(**color_func(WHITE)) # :3, : color white
        # ANIMATIONS
        anim_kwargs = {"run_time": 2}
        self.pause()
        self.play(Write(L1[:2]),**anim_kwargs)
        self.play(DrawBorderThenFill(temp_circle),**anim_kwargs)
        self.remove(temp_circle)
        self.add(cake)
        self.play(GrowFromCenter(right_line),**anim_kwargs)
        self.play(Write(dd3),**anim_kwargs)
        self.pause()
        self.play(
            TransformFromCopy(L1[0],L2[0]),
            TransformFromCopy(L1[1],L2[2]),
            **anim_kwargs
        )
        self.play(TransformFromCopy(dd3,dd3_),**anim_kwargs)
        self.pause()
        self.play(MoveToTarget(cake),**anim_kwargs)
        self.pause()
        self.play(TransformFromCopy(cake[SILCE_INDEX],slice),**anim_kwargs)
        self.pause()
        self.play(TransformFromCopy(L2,L3),**anim_kwargs)
        self.pause()
        self.play(
            L1.animate.shift(UP).fade(1),
            L2.animate.set_y(previus_mobs[0].get_y()),
            L3.animate.set_y(previus_mobs[-1].get_y()),
            **anim_kwargs
        )
        # self.pause()
        self.remove(L1)

        # self.add(L1,L2,L3)
        return VGroup(L2,L3)

    def sub_scene_3(self, pmobs: VGroup):
        pm1, pm2 = pmobs
        SCALE_TEX = self.SCALE_TEX
        STATIC_COLOR = BLUE
        max_height = max(*[mob.get_height() for mob in pm2])
        cake = CircleFraction(3)
        cake.set_height(max_height)\
            .set_color(BLUE)\
            .flip()
        slice = cake[1].deepcopy()
        slice.set_color(GREEN)
        slice_phantom = slice.deepcopy()
        slice_phantom.set_style(fill_opacity=0,stroke_opacity=0)
        n1 = Tex("1",color=STATIC_COLOR)\
            .arrange(RIGHT,buff=0.2)
        equal_sign = Tex("=")
        right_line = Line(UP,DOWN,color=RED)\
            .set_height(cake.get_height())
        _1_over_3 = Tex("\\frac{1}{3}",color=GREEN)
        dd_left = Tex(":")
        dd_right = dd_left.deepcopy()
        dd_left.set_fill(opacity=0)
        L1 = VGroup(
                n1,dd_left,slice_phantom,equal_sign,cake,right_line,dd_right,_1_over_3
            )\
            .arrange(RIGHT,buff=0.2)
        L1[-3:].shift(RIGHT*0.3)
        L1[-4:].shift(RIGHT*0.3)
        L1.to_edge(RIGHT,buff=1.2)
        # L1.shift(RIGHT*0.4)
        L1.align_to(pm1,UP)
        cake_without_pieces = Circle(
                radius=cake.get_height()/2,
                color=BLUE,
                fill_opacity=1,
            )\
            .move_to(cake)
        # Second Line
        L2 = L1[:5].deepcopy()
        [l.set_y(pm2[-1].get_y()) for l in L2]
        [L2[i].set_x(L1[i].get_x()) for i in range(len(L2))]
        L2[1:3].set_style(fill_opacity=1)
        # Third line
        L3 = L2.deepcopy()
        frac = pm2[-1][-1].deepcopy()
        frac.move_to(L3[2])
        line_distance = pm2[-1].get_y() - pm2[-2].get_y()
        L3[2].become(frac)
        L3.shift(UP*line_distance)
        _3 = Tex("3",color=RED)
        _3.move_to(L3[-1])
        L3.remove(L3[-1])
        L3.add(_3)
        # ANIMATIONS
        anim_kwargs = {"run_time": 2}
        # self.add(L1,cake_without_pieces,L2,L3,cake_numbers)
        # Write L1
        self.play(
            Write(VGroup(L1[0],L1[3],cake_without_pieces)),
            **anim_kwargs
        )
        self.wait(0.3)
        self.play(GrowFromCenter(right_line),**anim_kwargs)
        self.play(Write(L1[-2:]),**anim_kwargs)
        self.wait()
        # L1 -> L2
        temp_cake = cake_without_pieces.deepcopy()
        cake_ = L2[-1]
        # temp_cake.move_to(L2[4])
        self.play(
            TransformFromCopy(L1[0],L2[0]),
            TransformFromCopy(L1[3],L2[3]),
            temp_cake.animate.move_to(cake_),
            **anim_kwargs
        )
        self.wait()
        self.play(
            TransformFromCopy(L1[-2:],L2[1:3]),
            **anim_kwargs
        )
        self.wait()
        self.remove(temp_cake)
        self.add(cake_)
        cake_.generate_target()
        ct = cake_.target
        ct.separe_slices()
        ct.set_style(fill_opacity=0,stroke_opacity=1,stroke_width=1.5)
        cake_numbers = VGroup(*[
                Tex(f"{i+1}",color=RED)
                    .move_to(s)
                for i,s in enumerate(ct)
            ])
        self.play(MoveToTarget(cake_))
        self.wait(0.3)
        # self.play(Write(cake_numbers))
        self.play(Write(cake_numbers[0]))
        self.wait(0.8)
        self.play(Write(cake_numbers[1]))
        self.wait(0.8)
        self.play(Write(cake_numbers[2]))
        self.wait(0.8)
        self.pause()
        # L2 - L3
        self.play(
                TransformFromCopy(L2[:-1],L3[:-1]),
                TransformFromCopy(cake_numbers[-1],L3[-1]),
                **anim_kwargs
            )
        self.wait(1.5)
        # Shift and fade
        L1.add(cake_without_pieces)
        L2.add(cake_numbers)
        self.play(
            L1.animate.shift(UP).fade(1),
            L2.animate.set_y(pm2[0].get_y()),
            L3.animate.set_y(pm2[-1].get_y()),
            **anim_kwargs
        )
        # self.pause()
        self.remove(L1)
        return VGroup(L2,L3)

    def make_thumb_1(self, pm: VGroup):
        pm.generate_target()
        # Make targets
        pmt = pm.target
        rect = SurroundingRectangle(pm)\
            .set_color(WHITE)
        rect.generate_target()
        rt = rect.target
        # 
        pmt.scale(0.3)\
            .to_corner(DR)\
            .to_edge(DOWN,buff=0.15)
        rt\
            .set_width(pmt.get_width()+0.2)\
            .set_height(pmt.get_height()+0.2)\
            .move_to(pmt)\
        # Animation
        self.play(ShowCreation(rect))
        self.play(
            MoveToTarget(pm),
            MoveToTarget(rect),
            run_time=2.5
        )
        return VGroup(pm, rect)
    
    def sub_scene_4(self):
        # L1
        STATIC_COLOR = BLUE
        _n1 = Tex("1",color=STATIC_COLOR)
        _dd = Tex(":",color=WHITE)
        _1_over_3 = Tex(*get_frac_string(["1"],["3"]))
        _1_over_3[0].set_color(ORANGE)
        _1_over_3[-1].set_color(PURPLE)
        _eq = Tex("=")
        L1 = VGroup(_n1, _dd, _1_over_3, _eq)\
            .arrange(RIGHT,buff=0.4)
        L1.to_edge(LEFT,buff=0.8)
        L1.to_edge(UP,buff=1.2)
        # L2
        L2_empty = SurroundingRectangle(L1,buff=0)\
            .next_to(L1,DOWN)\
            .fade(1)
        _3_over_1 = Tex(*get_frac_string(["3"],["1"]))\
            .move_to([_1_over_3.get_x(),L2_empty.get_y(),0])
        _3_over_1[0].set_color(PURPLE)
        _3_over_1[-1].set_color(ORANGE)
        # L3
        _times = Tex("\\times",color=WHITE)
        L2 = VGroup(
                _n1.deepcopy(),
                _times,
                _3_over_1.deepcopy(),
                _eq.deepcopy()
            )
        y_distance = L2_empty.get_y() - L1.get_y()
        [l2.move_to([
                l1.get_x(),L2_empty.get_y()+y_distance,0
            ]) 
            for l2,l1 in zip(L2,L1)
        ]
        _3 = Tex("3")\
            .next_to(L2,RIGHT,buff=0.4)
        L2.add(_3)
        # ----- Animations
        ak = {"run_time": 2}
        # Write L1
        self.play(Write(L1),**ak)
        self.wait()
        _1_over_3_temp = _1_over_3.deepcopy()
        _dd_temp = _dd.deepcopy()
        _times_temp = _times.deepcopy()
        _times_temp.move_to([L2[1].get_x(),L2_empty.get_y(),0])
        # Write part L2
        # self.play(Write(VGroup(*L2[:2],L2[-2])),**ak)
        self.play(
            TransformFromCopy(L1[0],L2[0]),
            TransformFromCopy(L1[-1],L2[-2]),
            **ak
        )
        self.wait()
        # 1_over_3 down
        self.play(
            _dd_temp.animate.move_to(_times_temp),
            _1_over_3_temp.animate.move_to(_3_over_1),
            **ak
        )
        self.wait()
        # Flip
        _1o3 = _1_over_3_temp
        _3o1 = _3_over_1
        self.play(
            ReplacementTransform(_1o3[0],_3o1[-1],path_arc=PI),
            ReplacementTransform(_1o3[-1],_3o1[0],path_arc=PI),
            ReplacementTransform(_1o3[1],_3o1[1],path_arc=PI),
            ReplacementTransform(_dd_temp,_times_temp),
            **ak
        )
        self.wait()
        # temp L1 to L2
        self.play(
            ReplacementTransform(_times_temp,L2[1]),
            ReplacementTransform(_3o1,L2[2]),
            **ak
        )
        self.wait()
        self.play(Write(L2[-1]))
        self.wait()
        # Up L2
        self.play(L2.animate.set_y(L2_empty.get_y()))
        # TEST
        # self.add(L1,L2_empty,_3_over_1,L2)
        return VGroup(L1,L2)
    
    def sub_scene_5(self, pmobs):
        # L1
        STATIC_COLOR = BLUE
        COLOR_5 = ORANGE
        COLOR_2 = PURPLE
        TEX_BUFF = 0.2
        _n4 = Tex("4",color=STATIC_COLOR)
        _dd = Tex(":")
        _2_over_5 = Tex(*get_frac_string(["3"],["5"]))
        _eq = Tex("=")
        L1 = VGroup(_n4, _dd, _2_over_5, _eq)\
            .arrange(RIGHT,buff=TEX_BUFF)
        L1.to_edge(UP,buff=1.2)
        L1.shift(LEFT*1.8) # <-------------
        # L2
        L2_empty = SurroundingRectangle(L1,buff=0)\
            .next_to(L1,DOWN)\
            .fade(1)
        _5_over_2 = Tex(*get_frac_string(["5"],["3"]))\
            .move_to([_2_over_5.get_x(),L2_empty.get_y(),0])
        # L3
        _times = Tex("\\times")
        L2 = VGroup(
                _n4.deepcopy(),
                _times,
                _5_over_2.deepcopy(),
                _eq.deepcopy()
            )
        y_distance = L2_empty.get_y() - L1.get_y()
        [l2.move_to([
                l1.get_x(),L2_empty.get_y()+y_distance,0
            ]) 
            for l2,l1 in zip(L2,L1)
        ]
        # L3 
        _4t5o2 = Tex(*get_frac_string(["4","\\times","5"],["3"]))
        _20o2 = Tex(*get_frac_string(["20"],["3"]))
        _10 = Tex("10")
        L2_rest = VGroup(
                _4t5o2,
                _eq.deepcopy(),
                _20o2,
            )\
            .arrange(RIGHT,buff=TEX_BUFF)
        L2_rest.next_to(L2,RIGHT,TEX_BUFF)
        # COLORS
        _2_over_5[0].set_color(COLOR_2)
        _2_over_5[-1].set_color(COLOR_5)
        _5_over_2[0].set_color(COLOR_5)
        _5_over_2[-1].set_color(COLOR_2)
        _4t5o2[0].set_color(STATIC_COLOR)
        _4t5o2[2].set_color(COLOR_5)
        _4t5o2[-1].set_color(COLOR_2)
        _20o2[-1].set_color(COLOR_2)
        L2[2][0].set_color(COLOR_5)
        L2[2][-1].set_color(COLOR_2)
        # ----- Animations
        ak = {"run_time": 2}
        # Write L1
        self.play(Write(L1),**ak)
        self.wait()
        _2_over_5_temp = _2_over_5.deepcopy()
        _dd_temp = _dd.deepcopy()
        _times_temp = _times.deepcopy()
        _times_temp.move_to([L2[1].get_x(),L2_empty.get_y(),0])
        # Write part L2
        # self.play(Write(VGroup(*L2[:2],L2[-2])),**ak)
        self.play(
            TransformFromCopy(L1[0],L2[0]),
            TransformFromCopy(L1[-1],L2[-1]),
            **ak
        )
        self.wait()
        # 1_over_3 down
        self.play(
            _dd_temp.animate.move_to(_times_temp),
            _2_over_5_temp.animate.move_to(_5_over_2),
            **ak
        )
        self.wait()
        # Flip
        _2o5 = _2_over_5_temp
        _5o2 = _5_over_2
        self.play(
            ReplacementTransform(_2o5[0],_5o2[-1],path_arc=PI),
            ReplacementTransform(_2o5[-1],_5o2[0],path_arc=PI),
            ReplacementTransform(_2o5[1],_5o2[1],path_arc=PI),
            ReplacementTransform(_dd_temp,_times_temp),
            **ak
        )
        self.wait()
        # temp L1 to L2
        self.play(
            ReplacementTransform(_times_temp,L2[1]),
            ReplacementTransform(_5o2,L2[2]),
            **ak
        )
        self.wait()
        # 4 \times 5 -> 4 \times 5
        self.play(
            TransformFromCopy(L2[0][0],_4t5o2[0]),
            TransformFromCopy(L2[1][0],_4t5o2[1]),
            TransformFromCopy(L2[2][0],_4t5o2[2]),
            run_time=3
        )
        # -> frac line
        self.wait()
        self.play(
            TransformFromCopy(L2[2][1],_4t5o2[3]),
            **ak
        )
        # -> 2 denominator
        self.wait()
        self.play(
            TransformFromCopy(L2[2][2],_4t5o2[4]),
            **ak
        )
        self.wait()
        # Write equal
        self.play(Write(L2_rest[1]))
        self.wait()
        # 4 \times 5 -> 20
        self.play(
            FadeTransform(_4t5o2[:-2].deepcopy(),_20o2[0]),
            **ak
        )
        self.wait()
        # over 2 -> over 2
        self.play(
            TransformFromCopy(_4t5o2[-2:].deepcopy(),_20o2[1:]),
            **ak
        )
        self.wait()
        # 
        self.play(
            L2.animate.set_y(L2_empty.get_y()),
            L2_rest.animate.set_y(L2_empty.get_y()),
        )
        # self.wait()
        return VGroup(L1,VGroup(*L2,*L2_rest))
    
    def sub_scene_6(self, pmobs):
        # L1
        STATIC_COLOR = BLUE
        COLOR_5 = ORANGE
        COLOR_2 = PURPLE
        TEX_BUFF = 0.2
        _3o4 = Tex(*get_frac_string(["3"],["4"]),color=STATIC_COLOR)
        _dd = Tex(":")
        _2_over_5 = Tex(*get_frac_string(["2"],["5"]))
        _eq = Tex("=")
        L1 = VGroup(_3o4, _dd, _2_over_5, _eq)\
            .arrange(RIGHT,buff=TEX_BUFF)
        L1.to_edge(UP,buff=1.2)
        L1.to_edge(RIGHT,buff=3)
        # L2
        L2_empty = SurroundingRectangle(L1,buff=0)\
            .next_to(L1,DOWN)\
            .fade(1)
        _5_over_2 = Tex(*get_frac_string(["5"],["2"]))\
            .move_to([_2_over_5.get_x(),L2_empty.get_y(),0])
        # L3
        _times = Tex("\\times")
        L2 = VGroup(
                _3o4.deepcopy(),
                _times,
                _5_over_2.deepcopy(),
                _eq.deepcopy()
            )
        y_distance = L2_empty.get_y() - L1.get_y()
        [l2.move_to([
                l1.get_x(),L2_empty.get_y()+y_distance,0
            ]) 
            for l2,l1 in zip(L2,L1)
        ]
        _2_over_5[0].set_color(COLOR_2)
        _2_over_5[-1].set_color(COLOR_5)
        _5_over_2[0].set_color(COLOR_5)
        _5_over_2[-1].set_color(COLOR_2)
        # COLORS
        L2[2][0].set_color(COLOR_5)
        L2[2][-1].set_color(COLOR_2)
        # ----- Animations
        ak = {"run_time": 2}
        # Write L1
        self.play(Write(L1),**ak)
        self.wait()
        _2_over_5_temp = _2_over_5.deepcopy()
        _dd_temp = _dd.deepcopy()
        _times_temp = _times.deepcopy()
        _times_temp.move_to([L2[1].get_x(),L2_empty.get_y(),0])
        # Write part L2
        # self.play(Write(VGroup(*L2[:2],L2[-2])),**ak)
        self.play(
            TransformFromCopy(L1[0],L2[0]),
            TransformFromCopy(L1[-1],L2[-1]),
            **ak
        )
        self.wait()
        # 1_over_3 down
        self.play(
            _dd_temp.animate.move_to(_times_temp),
            _2_over_5_temp.animate.move_to(_5_over_2),
            **ak
        )
        self.wait()
        # Flip
        _2o5 = _2_over_5_temp
        _5o2 = _5_over_2
        self.play(
            ReplacementTransform(_2o5[0],_5o2[-1],path_arc=PI),
            ReplacementTransform(_2o5[-1],_5o2[0],path_arc=PI),
            ReplacementTransform(_2o5[1],_5o2[1],path_arc=PI),
            ReplacementTransform(_dd_temp,_times_temp),
            **ak
        )
        self.wait()
        # temp L1 to L2
        self.play(
            ReplacementTransform(_times_temp,L2[1]),
            ReplacementTransform(_5o2,L2[2]),
            **ak
        )
        self.wait()
        # L2 next
        _3t5o4t2 = Tex(*get_frac_string(["3","\\times","5"],["4","\\times","2"]))
        _15o8 = Tex(*get_frac_string(["15"],["8"]))
        L2_next = VGroup(
                _3t5o4t2,
                _eq.deepcopy(),
                _15o8
            ).arrange(RIGHT)
        _3t5o4t2.align_to(L2,LEFT)
        L2_next[1].set_x(_eq.get_x())
        _15o8.next_to(L2_next,RIGHT,buff=TEX_BUFF)
        # L2_next.shift(UP*y_distance)
        eq = _3t5o4t2
        eq[0].set_color(STATIC_COLOR)
        eq[1].set_x(_times.get_x())
        eq[5].set_x(_times.get_x())
        eq[2].set_x(_5_over_2.get_x()).set_color(COLOR_5)
        eq[-1].set_x(_5_over_2.get_x()).set_color(COLOR_2)
        eq[3].set_width(L1[:-1].get_width())
        eq[3].align_to(L1[:-1],LEFT)
        eq[4].set_color(STATIC_COLOR)
        L2_next.next_to(L2,RIGHT,buff=0.2)
        # -------------------------------------
        _3o4c = L2[0]
        _timesc = L2[1]
        _5o2c = L2[2]
        self.play(
            TransformFromCopy(_3o4c[0],eq[0]),
            TransformFromCopy(_3o4c[-1],eq[4]),
            TransformFromCopy(_3o4c[1].deepcopy(),eq[3]),
            # ---------------
            TransformFromCopy(_timesc[0],eq[1]),
            TransformFromCopy(_timesc[0],eq[-2]),
            # ---------------
            TransformFromCopy(_5o2c[0],eq[2]),
            TransformFromCopy(_5o2c[-1],eq[-1]),
            ReplacementTransform(_5o2c[1].deepcopy(),eq[3]),
            **ak
        )
        self.wait()
        self.play(Write(L2_next[1]))
        self.wait()
        self.play(
            ReplacementTransform(eq[:3].deepcopy(),_15o8[0]),
            ReplacementTransform(eq[3].deepcopy(),_15o8[1]),
            ReplacementTransform(eq[4:].deepcopy(),_15o8[-1]),
            **ak
        )
        self.wait()
        # self.remove(L2[-1])
        self.play(
            VGroup(L2,L2_next).animate.set_y(L2_empty.get_y()),
        )
        # self.wait()
        return VGroup(L1,L2,L2_next)
        
    def make_thumb_2(self, pm: VGroup):
        pm.generate_target()
        # Make targets
        pmt = pm.target
        rect = SurroundingRectangle(pm)\
            .set_color(WHITE)
        rect.generate_target()
        rt = rect.target
        # 
        pmt.scale(0.3)\
            .to_corner(DL)\
            .to_edge(DOWN,buff=0.15)
        rt\
            .set_width(pmt.get_width()+0.2)\
            .set_height(pmt.get_height()+0.2)\
            .move_to(pmt)\
        # Animation
        self.play(ShowCreation(rect))
        self.play(
            MoveToTarget(pm),
            MoveToTarget(rect),
            run_time=2.5
        )
        return VGroup(pm, rect)
    
    # Addition
    def sub_scene_7(self, pmob):
        SLICE_SCALE = 2
        _3_COLOR = ORANGE
        _4_COLOR = BLUE
        # First member
        wrc = {"cn": WHITE, "cd": RED}
        color_frac = lambda color: {"cn": WHITE, "cd": color}
        _1o3_slice = CircleFraction(3,color=_3_COLOR,fill_opacity=0)[1]
        _1o4_slice = CircleFraction(4,color=BLUE,fill_opacity=0)[0]
        slices = VGroup(
                _1o3_slice,
                _1o4_slice
            )\
            .scale(SLICE_SCALE)\
            .arrange(RIGHT,aligned_edge=UP,buff=0.8)\
            .to_edge(LEFT,0.8)
        _1o4_tex = get_frac_color(["1"],["4"],**color_frac(BLUE))\
            .move_to(_1o4_slice.get_center() + DL * 0.1)
        _1o3_tex = get_frac_color(["1"],["3"],**color_frac(_3_COLOR))\
            .move_to(_1o3_slice)\
            # .align_to(_1o4_tex,DOWN)
        fracs_tex_1 = VGroup(_1o3_tex,_1o4_tex)
        _sum = Tex("+")\
            .scale(1.7)\
            .move_to(slices)\
            .align_to(_1o4_slice,DOWN)
        _eq1 = Tex("=")\
            .scale(1.7)\
            .next_to(slices,RIGHT,0.8)\
            .align_to(_1o4_slice,DOWN)
        # Second member
        _12_slice = CircleFraction(12,stroke_width=0,add_stroke=True)\
            .rotate(PI/2)\
            .scale(SLICE_SCALE)\
            .next_to(_eq1,RIGHT,buff=0.8)\
            .align_to(slices,UP)
        _12_slice.flip(RIGHT)
        _7o12_slice = _12_slice[:7]
        _7o12_slice.add(_12_slice.strokes[:7])
        _3o12_slice = _7o12_slice[-4:-1].set_color(BLUE)
        _4o12_slice = _7o12_slice[:4].set_color(_3_COLOR)
        slices_strokes = _12_slice.strokes[:7][::-1]
        _1o4_slice_c = _1o4_slice.deepcopy()
        _1o3_slice_c = _1o3_slice.deepcopy()
        BUFFER_CAKES = 0.01
        _12_slice[:7].shift(LEFT*BUFFER_CAKES)
        slices_strokes[3:].shift(LEFT*BUFFER_CAKES)
        slices_strokes.set_style(stroke_width=1)
        s1 = slices_strokes[:3].set_color(BLUE)
        s2 = slices_strokes[3:].set_color(_3_COLOR)
        VGroup(_1o4_slice_c,_1o3_slice_c).set_stroke(width=1)
        _12_grp = VGroup(*[
            Tex("\\frac{1}{12}")
                .scale(0.4)
                .move_to(get_positition_to_tex(s,0.65))
            for s in slices_strokes
        ])
        _12_grp.rotate(-3*DEGREES,about_point=_12_slice.get_center())
        _1to4_numbers = VGroup(*[
            Tex(f"{i+1}",color=BLUE)
                .scale(0.5)
                .move_to(get_positition_to_tex(s,1,0.2))
            for i,s in enumerate(slices_strokes[3:])
        ])
        _1to3_numbers = VGroup(*[
            Tex(f"{i+1}",color=_3_COLOR)
                .scale(0.5)
                .move_to(get_positition_to_tex(s,1,0.2))
            for i,s in enumerate(slices_strokes[:3][::-1])
        ])
        for s in s1:
            s[0].set_color(BLUE)
            s[1].set_stroke(width=3)
            s[-1].set_color(_3_COLOR)
        for s in s2:
            s[0].set_color(_3_COLOR)
            s[1].set_stroke(width=3)
            s[-1].set_color(BLUE)
        s1[0].set_color(BLUE)
        s2[0].set_color(_3_COLOR)
        s1[0][-1].set_stroke(width=3)
        s2[-1][0].set_stroke(width=3)
        first_row = VGroup(
            slices, fracs_tex_1, _sum, _eq1,
            slices_strokes, _1o3_slice_c, _1o4_slice_c,
            _1to4_numbers,_1to3_numbers, _12_grp, #_4o12_slice, _3o12_slice
        )
        _eq2_7o12 = VGroup(
                Tex("=").scale(1.7),get_frac_color(["7"],["12"],WHITE,PURPLE)
            )\
            .arrange(RIGHT,buff=0.5)
        _eq2_7o12.next_to(first_row,RIGHT,buff=0.9)\
            .set_y(_eq1.get_y())
        first_row.add(_eq2_7o12)
        # self.add(third_row,_eq_7o12)
        # Animations
        ak = {"run_time": 2.5}
        self.play(
            DrawBorderThenFill(slices[0]),
            Write(fracs_tex_1[0])
        )
        self.pause()
        self.play(Write(_sum))
        self.pause()
        self.play(
            DrawBorderThenFill(slices[1]),
            Write(fracs_tex_1[1])
        )
        self.play(Write(_eq1))
        # self.pause()
        self.pause()
        self.add(_1o3_slice_c,_1o4_slice_c)
        self.bring_to_back(_1o3_slice_c)
        self.bring_to_back(_1o4_slice_c)
        self.play(
            _1o3_slice_c.animate.move_to(_4o12_slice),
            _1o4_slice_c.animate.move_to(_3o12_slice),
            **ak
        )
        self.pause()
        self.play(Write(slices_strokes[:3]),**ak)
        self.pause()
        self.play(Write(_1to3_numbers),**ak)
        self.pause()
        self.play(Write(slices_strokes[3:]),**ak)
        self.pause()
        self.play(Write(_1to4_numbers),**ak)
        self.pause(2)
        self.play(Write(_12_grp),**ak)
        self.pause()
        self.play(Write(_eq2_7o12),**ak)
        self.pause(2)
        self.play(first_row.animate.to_edge(UP))
        # Second row
        # ---------------------------------------
        first_member = VGroup(
                get_frac_color(["1"],["3"],WHITE,_3_COLOR),
                Tex("+"),
                get_frac_color(["1"],["4"],WHITE,BLUE),
                Tex("=").scale(1.7)
            )
        DOWN_BUFF = 0.5
        first_member.next_to(slices,DOWN,buff=DOWN_BUFF)
        [   f.set_x(s.get_x()) 
            for f,s in 
            zip(first_member,[fracs_tex_1[0],_sum,fracs_tex_1[1],_eq1])
        ]
        _1t4o3t4 = Tex(*get_frac_string(["1","\\times","4"],["3","\\times","4"]))
        _1t4o3t4[-1].set_color(BLUE)
        _1t4o3t4[2].set_color(BLUE)
        _1t4o3t4[-3].set_color(_3_COLOR)
        _1t3o4t3 = Tex(*get_frac_string(["1","\\times","3"],["4","\\times","3"]))
        _1t3o4t3[-1].set_color(_3_COLOR)
        _1t3o4t3[2].set_color(_3_COLOR)
        _1t3o4t3[-3].set_color(BLUE)
        second_member = VGroup(_1t4o3t4,Tex("+"),_1t3o4t3)\
            .arrange(RIGHT,buff=0.3)\
            .next_to(first_member,RIGHT,buff=0.9)
        # self.add(first_member,second_member)
        second_row = VGroup(first_member,second_member)
        # Third row
        third_row = VGroup(
            Tex("=").scale(1.7),
            get_frac_color(["4"],["12"],WHITE,PURPLE),
            Tex("+"),
            get_frac_color(["3"],["12"],WHITE,PURPLE),
        )
        _eq_7o12 = VGroup(Tex("=").scale(1.7),get_frac_color(["7"],["12"],WHITE,PURPLE))
        _eq_7o12.arrange(RIGHT,buff=0.5)
        third_row.next_to(second_member,DOWN,buff=DOWN_BUFF)
        [
            t.set_x(s.get_x())
            for t,s in zip(
                third_row,
                [first_member[-1],*second_member]
            )
        ]
        _eq_7o12.next_to(third_row,RIGHT,buff=0.9)
        # Animations -------------------------------------
        # -- DOWN first member
        self.play(*[
            TransformFromCopy(i,j)
            for i,j in zip(
                [fracs_tex_1[0],_sum,fracs_tex_1[1],_eq1],
                first_member
            )
        ],**ak)
        self.pause()
        # Transform 1/3 frac to second membmer
        self.play(
                *[
                    TransformFromCopy(i,j)
                    for i,j in zip(
                        first_member[0],
                        [_1t4o3t4[0],_1t4o3t4[3],_1t4o3t4[4]]
                    )
                ],
                *[
                    TransformFromCopy(i,j)
                    for i,j in zip(
                        first_member[2],
                        [_1t3o4t3[0],_1t3o4t3[3],_1t3o4t3[4]]
                    )
                ],
                TransformFromCopy(first_member[1],second_member[1]),
                run_time=3
            )
        self.pause()
        self.play(
            *[
                Write(_1t4o3t4[i])
                for i in [1,-2]
            ],
            *[
                Write(_1t3o4t3[i])
                for i in [1,-2]
            ],
            **ak
        )
        self.pause()
        self.play(
            ReplacementTransform(_1to4_numbers[-1][0].copy(),_1t4o3t4[2]),
            ReplacementTransform(_1to4_numbers[-1][0].copy(),_1t4o3t4[-1]),
            run_time=3
        )
        self.pause()
        self.play(
            ReplacementTransform(_1to3_numbers[-1][0].copy(),_1t3o4t3[2]),
            ReplacementTransform(_1to3_numbers[-1][0].copy(),_1t3o4t3[-1]),
            run_time=3
        )
        self.pause()
        # Three row
        self.play(
            TransformFromCopy(first_member[-1],third_row[0]),
            TransformFromCopy(second_member[1],third_row[2]),
            **ak
        )
        self.pause()
        # Transform fractions
        self.play(
            FadeTransform(_1t4o3t4.copy(),third_row[1]),
            **ak
        )
        self.pause()
        self.play(
            FadeTransform(_1t3o4t3.copy(),third_row[3]),
            **ak
        )
        self.pause()
        # Show result 7/12
        self.play(Write(_eq_7o12[0]))
        self.pause()
        self.play(
            FadeTransform(third_row[1:].copy(),_eq_7o12[1]),
            **ak
        )
        # self.pause()
        third_row.add(_eq_7o12)
        return VGroup(first_row, second_row, third_row)

        # self.add(_12_grp,_1to4_numbers,_1to3_numbers)
    
    def shrink_sum(self, pmob: VGroup):
        r1,r2,r3 = pmob
        rects = VGroup(*[
            SurroundingRectangle(r,buff=0).fade(1)
            for r in pmob
        ])
        max_width = max(*[w.get_width() for w in rects])
        [
            r.set_width(max_width,stretch=True).set_x(r1.get_x()) 
            for r in rects
        ]
        gr = VGroup(*[
            VGroup(p,r)
            for p,r in zip(pmob, rects)
        ])
        self.play(
            gr.animate
                .arrange(DOWN,buff=0.1,aligned_edge=LEFT)
                .scale(0.5)
                .to_edge(UP,buff=0.1),
            run_time=2.5
        )
        return pmob
    
    def sub_scene_8(self, pmob: VGroup, fracs=[[5,7],[3,8]], op=1, colors=[RED,BLUE,PURPLE], end_direction=LEFT, fix_func=lambda x: x, shift_end=True):
        up_line = Underline(pmob)\
            .set_width(FRAME_WIDTH)
        up_line.fade(1)
        self.add(up_line)
        COLOR_1= colors[0]
        COLOR_2= colors[1]
        COLOR_3= colors[2]
        frac1 = fracs[0]
        frac2 = fracs[1]
        frac1_prod = [frac1[0]*frac2[1],frac1[1]*frac2[1]]
        frac2_prod = [frac2[0]*frac1[1],frac2[1]*frac1[1]]
        operation = op
        op_tex = "+" if operation > 0 else "-"
        # ---------------------------
        first_row = VGroup(
                get_frac_color([f"{frac1[0]}"],[f"{frac1[1]}"],WHITE,COLOR_1),
                Tex(op_tex),
                get_frac_color([f"{frac2[0]}"],[f"{frac2[1]}"],WHITE,COLOR_2),
                Tex("=")
            )\
            .arrange(RIGHT,buff=0.7)
        DOWN_BUFF = 0.5
        first_row.next_to(up_line,DOWN)
        # first_member.next_to(slices,DOWN,buff=DOWN_BUFF)
        _1t4o3t4 = Tex(*get_frac_string([f"{frac1[0]}","\\times",f"{frac2[1]}"],[f"{frac1[1]}","\\times",f"{frac2[1]}"]))
        _1t4o3t4[-1].set_color(COLOR_2)
        _1t4o3t4[2].set_color(COLOR_2)
        _1t4o3t4[-3].set_color(COLOR_1)
        _1t3o4t3 = Tex(*get_frac_string([f"{frac2[0]}","\\times",f"{frac1[1]}"],[f"{frac2[1]}","\\times",f"{frac1[1]}"]))
        _1t3o4t3[-1].set_color(COLOR_1)
        _1t3o4t3[2].set_color(COLOR_1)
        _1t3o4t3[-3].set_color(COLOR_2)
        second_row = VGroup(_1t4o3t4,Tex(op_tex),_1t3o4t3,Tex("="))\
            .next_to(first_row,DOWN,buff=0.2)
        [
            r.set_x(s.get_x())
            for r,s in zip(second_row, first_row)
        ]
        second_row[1].set_y(_1t4o3t4[3].get_y())
        second_row[2][:3].align_to(second_row[2][-3:],RIGHT)
        second_row[0][:3].align_to(second_row[0][-3:],RIGHT)
        # Third row
        third_row = VGroup(
            get_frac_color([f"{frac1_prod[0]}"],[f"{frac1_prod[1]}"],WHITE,COLOR_3),
            Tex(op_tex),
            get_frac_color([f"{frac2_prod[0]}"],[f"{frac2_prod[1]}"],WHITE,COLOR_3),
            Tex("=")
        )
        third_row.next_to(second_row,DOWN,buff=0.2)
        [
            r.set_x(s.get_x())
            for r,s in zip(third_row,second_row)
        ]
        frac_final = [frac1_prod[0]+(frac2_prod[0]*operation),frac1_prod[1]+(frac2_prod[1]*operation)]
        result = get_frac_color(
                [f"{frac_final[0]}"],
                [f"{frac1_prod[1]}"],
                WHITE,COLOR_3
            )
        result.next_to(third_row,RIGHT,buff=0.4)
        # Fix things
        first_row[0].align_to(second_row[0],LEFT)
        first_row[2].align_to(second_row[2],LEFT)
        _eqs = VGroup(*[Tex("=").next_to(s,LEFT,buff=0.32) for s in [second_row,third_row]])
        _eqs[-1].set_x(_eqs[0].get_x())
        all_mobs = VGroup(first_row,second_row,third_row,result)
        fix_func(all_mobs)
        # ANIMATIONS
        self.play(Write(first_row),run_time=3)
        self.pause()
        # # Transform 1/3 frac to second membmer
        # self.play(Write(_eqs[0]))
        # self.pause()
        self.play(
                *[
                    TransformFromCopy(i,j)
                    for i,j in zip(
                        first_row[0],
                        [_1t4o3t4[0],_1t4o3t4[3],_1t4o3t4[4]]
                    )
                ],
                *[
                    TransformFromCopy(i,j)
                    for i,j in zip(
                        first_row[2],
                        [_1t3o4t3[0],_1t3o4t3[3],_1t3o4t3[4]]
                    )
                ],
                TransformFromCopy(first_row[1],second_row[1]),
                TransformFromCopy(first_row[-1],second_row[-1]),
                run_time=3
            )
        self.pause()
        self.play(
            *[
                Write(_1t4o3t4[i])
                for i in [1,-2]
            ],
            *[
                Write(_1t3o4t3[i])
                for i in [1,-2]
            ],
            run_time=1.5
        )
        self.pause()
        self.play(
            ReplacementTransform(first_row[2][-1].copy(),_1t4o3t4[2]),
            ReplacementTransform(first_row[2][-1].copy(),_1t4o3t4[-1]),
            run_time=3
        )
        self.pause()
        self.play(
            ReplacementTransform(first_row[0][-1].copy(),_1t3o4t3[2]),
            ReplacementTransform(first_row[0][-1].copy(),_1t3o4t3[-1]),
            run_time=3
        )
        self.pause()
        # # Three row
        self.play(
            # TransformFromCopy(_eqs[0],_eqs[1]),
            TransformFromCopy(second_row[1],third_row[1]),
            TransformFromCopy(second_row[-1],third_row[-1]),
            run_time=2
        )
        self.pause()
        # self.pause()
        # Transform fractions
        self.play(
            FadeTransform(_1t4o3t4.copy(),third_row[0]),
            run_time=2.3
        )
        self.pause()
        self.play(
            FadeTransform(_1t3o4t3.copy(),third_row[2]),
            run_time=2.3
        )
        self.pause()
        # # Show result
        # self.play(Write(_eq_7o12[0]))
        self.pause()
        self.play(
            FadeTransform(third_row[:len(third_row)-1].copy(),result),
            run_time=2.5
        )
        # self.pause(2)
        if shift_end:
            self.pause(2)
            self.play(all_mobs.animate.to_edge(end_direction),run_time=2)
            # self.wait()
        return all_mobs
        # third_row.add(_eq_7o12)
        # return VGroup(first_row, second_row, third_row)
    
    def sub_scene_9(self, pmob):
        pass
