from manimlib.imports import *

class Work2(Scene):
    CONFIG = {
        "axes_config": {
            "center_point": [-5.5,-2.7,0],
            "x_axis_config": {
                "x_min": -1,
                "x_max": 11,
                "include_numbers": True
            },
            "y_axis_config": {
                "label_direction": UP,
                "x_min": -1,
                "x_max": 6.5,
                "include_numbers": True
            },
        },
        "func1": lambda x: x/2,
        "func2": lambda t: (np.exp(t)-1)/1000,
        "func3": lambda t: (np.log(t/10+0.01)*1.3+5.8),
        "func_config1": {
            "color": RED,
            "x_min": 0,
            "x_max": 9,
        },
        "func_config2": {
            "color": RED,
            "x_min": 0,
            "x_max": 8.83,
        },
        "func_config3": {
            "color": GREEN,
            "x_min": 0.01,
            "x_max": 12.6,
        },
        "step_dot": 0,
    }
    def construct(self):
        time = DecimalNumber(self.time).to_edge(DL)
        time.add_updater(lambda mob: mob.set_value(self.time))
        axes = self.get_axes()
        func1 = self.get_graph(self.func1,**self.func_config1)
        func2 = self.get_graph(self.func2,  **self.func_config2)
        func3 = self.get_graph(self.func3,  **self.func_config3)
        dot = Dot(self.axes.c2p(0,0),color=YELLOW)
        first_step = self.get_step_triangle(0)
        fn1 = TextMobject("Linear learning").rotate(np.arctan(1/2))
        fn2 = TextMobject("Structural learning").next_to(func2,RIGHT,buff=0).shift(DOWN+LEFT*0.8)
        fn3 = TextMobject("Emotions").next_to(func3,UP,buff=0).shift(LEFT*1.5+DOWN*0.9)
        uv = Line(self.axes.c2p(0,0),self.axes.c2p(4,self.func1(4))).get_unit_vector()
        fn1.next_to(self.axes.c2p(4,self.func1(4)),uv + [0,1.3,0],buff=0.2)
        line = Line(self.axes.c2p(0,0),self.axes.c2p(0.0000001,0.0000001))
        line.step = 0 
        line.add_updater(self.get_updater_linear_line(line))
        self.axes[0][-1].fade(1)
        self.axes[1][-1].fade(1)
        self.axes[1][-3].fade(1)
        self.axes[0][-3].fade(1)
        up_tip = self.axes[1][1]
        right_tip = self.axes[0][1]
        x_axis_name = TextMobject("Time").next_to(right_tip,RIGHT)
        y_axis_name = TextMobject("Knowledge").next_to(up_tip,RIGHT)
        self.add(axes,line,x_axis_name,y_axis_name)
        self.bring_to_back(first_step)
        self.play(
            FadeIn(first_step)
        )
        self.step_animation(0)
        self.step_animation(0.5)
        self.step_animation(1)
        self.wait(7)
        func2.save_state()
        self.play(
            Write(fn1)
        )
        self.play(
            ShowCreation(func2,run_time=5)
        )
        self.play(Write(fn2))
        self.play(
            FadeOut(func2)
        )
        func2.restore()
        self.play(
            ShowCreation(func2,run_time=4),
            AnimationGroup(
                Animation(Mobject(),run_time=0.3),
                ShowCreation(func3,run_time=5),
                lag_ratio=1,
            ),
        )
        self.play(Write(fn3))
        self.wait(5)

    def get_axes(self):
        self.axes = Axes(**self.axes_config)
        # FIX Y LABELS
        y_labels = self.axes.get_y_axis().numbers
        for label in y_labels:
            label.rotate(-PI/2)
        return self.axes

    def get_graph(self,func,**kwargs):
        return self.axes.get_graph(
                                    func,
                                    **kwargs
                                )

    def get_step_triangle(self,step):
        return Polygon(
            self.axes.c2p(step, self.func1(step)),
            self.axes.c2p(step+0.5, self.func1(step)),
            self.axes.c2p(step+0.5, self.func1(step+0.5)),
        )

    def step_animation(self, step):
        pre = self.get_step_triangle(step)
        post = self.get_step_triangle(step+0.5)
        self.add(pre,post)
        self.bring_to_back(pre)
        self.bring_to_back(post)
        self.play(
            TransformFromCopy(
                pre, post
            )
        )

    def get_updater_linear_line(self, mob):
        self.speed_grow = 0.5
        def updater(mob, dt):
            if mob.step > 2:
                self.speed_grow += 0.02
            if mob.get_end()[0] > FRAME_X_RADIUS:
                self.speed_grow = 0
                mob.clear_updaters()
            mob.step += dt * self.speed_grow
            mob.become(
                Line(
                    self.axes.c2p(0,0),
                    self.axes.c2p(mob.step,self.func1(mob.step))
                )
            )
        return updater
