from manimlib.imports import *
from projects.tb_animations import Lights

try:                 # height, fps
    set_custom_quality(700, 20)
except:
    pass

# CONSTANTS
HUMAN_COLOR = "#974F9F"
BEAST_COLOR = "#FA841C"

class Human(MovingCameraScene):
    CONFIG = {
        # HUMAN-BEAST CONFIG
        "human_beast_svg": "head",
        "human_beast_initial_position": ORIGIN + RIGHT * 0.3,
        "human_beast_config": {
            "stroke_width": 0,
            "sheen_direction": RIGHT
        },
        "human_beast_svg_index": 1,
        "human_beast_gradient_colors": [
            HUMAN_COLOR,
            BEAST_COLOR,
        ],
        "human_beast_initial_height": 3,
        # BRAIN CONFIG
        "brain_svg": "brain",
        "brain_config": {
            "color": WHITE,
            "fill_opacity": 0.7,
            "stroke_width": 0
        },
        # DON'T CHANGE THIS
        "initial_alpha": {
            "alpha_x": -0.77,
            "alpha_y": 0.02,
        },
        "beast_alpha": {
            "alpha_x": 0.65,
            "alpha_y": 0.05,
        },
        "camera_class": MovingCamera,
        "branch_config": {
            "angle": 35*DEGREES,
            "scale": 0.7
        },
        "random_seed": 1,
        "alphas": {
            "left": {
                "x": -0.77,
                "y": 0.01
            },
            "right": {
                "x": 0.4,
                "y": 0.5
            }
        }
    }
    def construct(self):
        # FIRST SCENE
        self.import_svg()
        self.show_svg()
        self.show_branch()
        self.examination()
        self.show_brain()
        self.move_first_branch_to_brain()
        self.show_second_branch()
        self.third_branch()
        self.fourth_branch()
        self.fifth_branch()
        self.several_branches()
        print("Start second part")
        # SECOND SCENE
        self.move_head_and_remove_brain()
        self.show_tree()
        self.show_fractal()
        self.wait(9)

    # SECOND 
    # SCENE
    #-------------------------------------------------------
    def move_head_and_remove_brain(self):
        self.remove(self.back_brain)
        brain = self.brain.copy()
        self.play(
            *[Uncreate(mob) for mob in VGroup(
                #self.brain,
                self.first_branch,
                self.second_branch,
                self.third_branch,
                self.fourth_branch,
                self.fifth_branch,
                self.branches,
            )],
            FadeOut(self.left_lights)
        )
        self.wait()
        self.brain = None
        self.brain = brain
        self.beast_eyes = Circle(
            fill_opacity=1,
            stroke_width=0,
            radius=0.1,
            sheen_direction=RIGHT
        )
        self.play(
            Restore(self.human)
        )
        self.beast_eyes.set_color(color=[WHITE,BLACK])
        self.beast_eyes.move_to(
            self.human_beast.get_center() + \
            self.human_beast_initial_height * self.beast_alpha["alpha_x"] * RIGHT + \
            self.human_beast_initial_height * self.beast_alpha["alpha_y"] * UP
        )
        self.wait()
        self.play(
            LaggedStartMap(FadeIn,self.human_beast[36:])
        )
        self.wait()
        self.beast_lights = Lights(self.beast_eyes,size=0.01,flip=True,angle=40*DEGREES)
        self.add(self.beast_lights)
        self.bring_to_back(self.beast_lights)
        self.play(
            UpdateFromAlphaFunc(self.beast_lights,self.show_lights(self.beast_lights,max_size=2))
        )
        self.wait()
        #self.human_beast

    def show_tree(self):
        self.tree_svg = SVGMobject(
            "Green_v2",
            stroke_width=0,
            color="#A8CF45",
            height=2.5,
        )
        self.tree_svg.to_edge(RIGHT,buff=0.2)
        tree = self.tree_svg
        self.play(DrawBorderThenFill(tree))
        self.wait()
        fractal = VGroup(
            Line(DOWN,ORIGIN)
        )
        fractal_group = VGroup()
        self.get_fractal(fractal, fractal_group)
        #self.add(fractal)
        tree_brain = tree.copy()
        tree_brain.scale(0.8)
        tree_brain.next_to(self.human_beast,UP)
        self.play(
            TransformFromCopy(tree,tree_brain,path_arc=-PI/4)
        )
        self.wait()
        fractal.set_height(tree_brain.get_height()*0.9)
        fractal.next_to(tree_brain.get_bottom(),UP,buff=0)
        fractal_group.match_height(fractal)
        fractal_group.move_to(fractal)
        fractal_group.set_color(GREEN_E)
        self.tree_brain_svg = tree_brain
        self.fractal = fractal
        self.fractal_group = fractal_group
        #self.add(fractal_group)

    def show_fractal(self):
        bg = self.fractal_group.copy()
        bg.set_color(WHITE)
        point = Dot().set_x(-FRAME_WIDTH*1.2/4)
        list_group = self.get_list_group(3)
        left_eye = Dot(radius=0.1)
        left_eye.move_to(
            self.human_beast.get_center() + \
            self.human_beast.get_height() * self.alphas["left"]["x"] * RIGHT + \
            self.human_beast.get_height() * self.alphas["left"]["y"] * UP
        )
        self.left_lights = Lights(left_eye,size=0.01,angle=40*DEGREES)
        self.add_sound("music")
        self.play(
            UpdateFromAlphaFunc(self.left_lights,self.show_lights(self.left_lights,max_size=2))
        )

        random_sizes = [1, 2, 3, 4, 5]
        random.shuffle(random_sizes)
        random.shuffle(list_group)
        random_sizes = it.cycle(random_sizes)
        self.wait()
        step = 0
        #print(len(list_group))
        list_group2 = [
            *list_group[:4],
            list_group[4:4+3],
            list_group[4+3:4+3+5],
            list_group[4+3+5:4+3+5+12],
        ]
        #print(list_group2)
        for i in list_group2:
            if step < 4 :
                target = self.fractal_group[i].copy()
                copy = target.copy()
                copy.set_color(WHITE)
                copy.scale(next(random_sizes))
                copy.move_to(point)
                self.play(ShowCreation(copy,run_time=1.5))
                target.set_color(BLACK)
                self.wait(0.3)
                self.play(Transform(copy,target,run_time=2))
            if step >= 4:
                group = VGroup()
                target_group = VGroup()
                for j in i:
                    group.add(
                        self.fractal_group[j].copy().scale(next(random_sizes))
                    )
                    target_group.add(
                        self.fractal_group[j]
                    )
                    target_group.set_color(BLACK)
                group.set_color(WHITE)
                group.arrange_in_grid(2)
                group.move_to(point)
                if step >= 5:
                    group.to_edge(LEFT,buff=0.2)
                self.play(ShowCreation(group,run_time=1.2))
                self.wait(0.3)
                self.play(*[
                    Transform(g,t,run_time=1.5)
                    for g,t in zip(group,target_group)
                ])
            # if step > 4:
            #     break
            step += 1
        self.wait()

    # FIRST
    # SCENE
    # ----------------------------------------------------
    def import_svg(self):
        pre_human_beast = SVGMobject(self.human_beast_svg,**self.human_beast_config)
        pre_human_beast.set_color(color=self.human_beast_gradient_colors)
        pre_human_beast.set_height(self.human_beast_initial_height)
        self.human_beast = pre_human_beast
        #self.human_beast.append_points([self.human_beast.points[-1],self.human_beast.points[0]])
        self.human_beast.move_to(self.human_beast_initial_position)
        # EYES
        human_eyes = Circle(
            fill_opacity=1,
            stroke_width=0,
            radius=0.1,
            sheen_direction=RIGHT
        )
        human_eyes.set_color(color=[BLACK,WHITE])
        human_eyes.move_to(
            self.human_beast.get_center() + \
            self.human_beast_initial_height * self.initial_alpha["alpha_x"] * RIGHT + \
            self.human_beast_initial_height * self.initial_alpha["alpha_y"] * UP
        )
        self.human_eyes = human_eyes
        colors = self.color_list()
        step = 0
        for mob in self.human_beast:
            try:
                mob.set_color(colors[step])
            except:
                break
            step += 1

    def examination(self):
        human_group = VGroup(
            self.human_eyes,
            self.human_beast,
        )
        human_group.save_state()
        self.human_eyes.set_fill(opacity=0)
        coord_zoom = (self.first_branch.get_x() + human_group.get_x())/2
        coord_zoom = [coord_zoom, 0, 0]
        self.human_eyes.set_fill(opacity=1)
        self.wait()
        self.human_group = human_group


    def show_branch(self):
        left_eye = Dot(radius=0.1)
        left_eye.move_to(
            self.human_beast.get_center() + \
            self.human_beast.get_height() * self.alphas["left"]["x"] * RIGHT + \
            self.human_beast.get_height() * self.alphas["left"]["y"] * UP
        )
        left_eye.shift(RIGHT*0.58)
        lights = Lights(left_eye,size=0.01,angle=40*DEGREES)
        self.left_lights = lights
        self.first_branch = self.get_branch(1/3.5, PI/5)
        self.first_branch.set_x(-FRAME_WIDTH*1.2/4)
        self.first_branch.scale(0.3)
        self.first_branch.rotate(30*DEGREES)
        self.play(
            UpdateFromAlphaFunc(self.left_lights,self.show_lights(lights,max_size=2))
        )
        self.wait()
        self.play(ShowCreation(self.first_branch))
        self.wait()

    def move_first_branch_to_brain(self):
        self.first_branch.generate_target()
        target = self.first_branch.target
        target.move_to(self.brain.get_center() + LEFT + UP*0.3)
        target.set_stroke(opacity=0.7)
        self.play(
            MoveToTarget(self.first_branch,path_arc=-PI/2),
        )
        self.wait()

    def show_svg(self):
        #self.play((self.human_beast,RIGHT,run_time=3))
        self.human = self.human_beast[:36]
        self.human.save_state()
        self.human.move_to(ORIGIN)
        self.play(
            LaggedStartMap(FadeIn,self.human)
        )
        # FIRST WAIT
        self.wait()

    def show_second_branch(self):
        self.second_branch = self.get_branch(1/2.4, PI/5)
        self.second_branch.scale(0.5)
        self.second_branch.set_x(-FRAME_WIDTH*1.2/4)
        self.second_branch.rotate(-40*DEGREES)
        self.play(
            ShowCreation(self.second_branch),
        )
        self.wait()
        self.second_branch.generate_target()
        target = self.second_branch.target
        target.match_height(self.first_branch)
        target.next_to(self.first_branch,RIGHT,buff=0.3)
        target.set_stroke(opacity=0.7)
        self.play(
            MoveToTarget(self.second_branch, path_arc=-PI/2),
        )
        self.wait()

    def third_branch(self):
        self.third_branch = self.get_branch(1/5, PI/4)
        self.third_branch.scale(1.7)
        self.third_branch.set_x(-FRAME_WIDTH*1.2/4)
        self.third_branch.rotate(-20*DEGREES)
        self.play(
            ShowCreation(self.third_branch)
        )
        self.wait()
        self.third_branch.generate_target()
        target = self.third_branch.target
        target.set_height(self.second_branch.get_height())
        target.next_to(self.second_branch,RIGHT,buff=0.3)
        target.set_stroke(opacity=0.7)
        self.bring_to_back(self.human_eyes)
        self.play(
            MoveToTarget(self.third_branch, path_arc=-PI/2),
        )
        self.wait()

    def fourth_branch(self):
        self.fourth_branch = self.get_branch(1/3, PI/8)
        self.fourth_branch.scale(1.5)
        self.fourth_branch.set_x(-FRAME_WIDTH*1.2/4)
        self.fourth_branch.rotate(50*DEGREES)
        self.play(
            ShowCreation(self.fourth_branch)
        )
        self.wait()
        self.fourth_branch.generate_target()
        target = self.fourth_branch.target
        target.set_height(self.third_branch.get_height())
        target.next_to(self.third_branch,RIGHT,buff=0.3)
        target.set_stroke(opacity=0.7)
        self.play(
            MoveToTarget(self.fourth_branch, path_arc=-PI/2),
        )
        self.wait()

    def fifth_branch(self):
        self.fifth_branch = self.get_branch(1/3, PI/4)
        self.fifth_branch.scale(1.8)
        self.fifth_branch.set_x(-FRAME_WIDTH*1.2/4)
        self.fifth_branch.rotate(-10*DEGREES)
        self.play(
            ShowCreation(self.fifth_branch)
        )
        self.wait()
        self.fifth_branch.generate_target()
        target = self.fifth_branch.target
        target.set_height(self.fourth_branch.get_height()*1.7)
        target.next_to(self.fourth_branch,LEFT,buff=0.5)
        target.rotate(PI/7)
        target.set_stroke(opacity=0.7)
        target.scale(0.3)
        target.shift(UP*0.5)
        self.play(
            MoveToTarget(self.fifth_branch, path_arc=-PI/2),
            FadeOut(self.human_eyes),
        )
        self.wait()

    def several_branches(self):
        self.branches = VGroup(
            self.get_branch(1/3, PI/4, size=0.4),
            self.get_branch(1/2, PI/7, size=0.6),
            self.get_branch(1/4, PI/6, size=1.2),
            self.get_branch(1/5, PI/6, size=1.3),
        )
        self.branches.set_x(-FRAME_WIDTH*1.2/4)
        buffs = [
            LEFT*0.5+DOWN*0.5,
            LEFT*0.6+DOWN*0.6,
            LEFT*0.7+DOWN*0.7,
            LEFT*0.5
        ]
        angles = [
            PI/6,
            -PI/7,
            PI/8,
            -PI/9
        ]
        step = 0
        for mob,coord,angle in zip(self.branches,buffs,angles):
            mob.generate_target()
            mob.target.scale(0.5)
            if step == 0:
                mob.target.scale(3.5)
            if step == 1:
                mob.target.scale(3)
            if step == 2:
                mob.target.scale(0.3)
            if step == 3:
                mob.target.scale(0.3)
            mob.target.move_to(self.fourth_branch.get_center()+coord)
            mob.target.rotate(angle)
            mob.target.set_stroke(opacity=0.7)
            self.play(ShowCreation(mob),run_time=0.7)
            self.play(MoveToTarget(mob, path_arc=-PI/2),run_time=0.7)
            step += 1
        self.wait()

    def get_branch(self,proportion=1/2,angle=PI/4,size=1,scale=1):
        main_line = Line(DOWN*size,UP*size,color=YELLOW)
        up_line = Line(
            main_line.get_start(),
            main_line.point_from_proportion(proportion)
        )
        down_line = Line(
            main_line.point_from_proportion(proportion),
            main_line.get_end(),
        )
        branch = VGroup(
            main_line,
            down_line,
            up_line
        )
        branch.rotate(PI)
        left_line = up_line.copy()
        right_line = up_line.copy()
        left_line.rotate(angle,about_point=left_line.get_end())
        right_line.rotate(-angle,about_point=right_line.get_end())
        branch.add(left_line,right_line)
        branch.remove(main_line,up_line)
        for mob in branch:
            mob.set_points(mob.points[::-1])
        branch.scale(scale)
        return branch

    def show_brain(self):
        self.brain = SVGMobject("brain2",**self.brain_config)
        self.back_brain = SVGMobject("brain",**self.brain_config)[0][0]
        self.brain.to_edge(UP,buff=0.3)
        #self.add(self.brain)
        self.brain.flip(axis=RIGHT)
        self.brain.move_to(self.brain)
        self.brain.scale(1.1)
        self.back_brain.flip(axis=RIGHT).set_height(self.brain.get_height()*0.98).move_to(self.brain)
        self.brain.set_style(fill_opacity=1,stroke_width=2,stroke_color=WHITE,fill_color=BLACK)
        self.play(
            Succession(
                ShowCreation(self.brain),
                FadeOut(self.brain)
            )
        )
        self.wait()

    def get_branches(self,main,group,angle,scale):
        def get_branch(mob,vector,scale):
            return Line(
                mob.get_end(),
                mob.get_end() + vector * mob.get_length() * scale,
            )
        unit_vector = main.get_unit_vector()
        first_branch_vector  = rotate_vector(unit_vector, angle)
        second_branch_vector = rotate_vector(unit_vector, -angle)
        first_branch = get_branch(main, first_branch_vector,scale)
        second_branch = get_branch(main, second_branch_vector,scale)
        group.add(
            VGroup(main,first_branch,second_branch)
        )
        return [first_branch, second_branch]

    def get_list_group(self, n):
        pre_list = [
            list(range(4**n - 1,2 * 4 ** n - 1))
            for n in range(n)
        ]
        list_expanded = []
        for i in range(len(pre_list)):
            for j in pre_list[i]:
                list_expanded.append(j)
        return list_expanded

    def get_fractal(self, branch, branch_group, level=5):
        n2 = [2**n for n in range(level)]
        for n in n2:
            for mob in branch[-n:]:
                branch.add(*self.get_branches(mob,branch_group,**self.branch_config))

    def color_list(self):
        import csv
        colors = []
        with open('projects/head_colors.csv', newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
            for row in spamreader:
                for color in row:
                    colors.append(color)
        return colors

    def set_color_head(self,svg):
        step = 0
        colors = self.color_list()
        for mob in svg:
            try:
                mob.set_color(colors[step])
            except:
                break
            step += 1

    def show_lights(self, lights, max_size):
        min_size = lights.size
        circle = Circle(radius=lights.radius)
        circle.move_to(lights.position)
        def update_lights(mob,alpha):
            size = interpolate(min_size,max_size,alpha)
            mob.become(
                Lights(circle,size=size,angle=lights.angle,color=lights.color, flip=lights._flip)
            )
        return update_lights
# TESTS-----------------------------------------------------------
class TestSVG(Scene):
    CONFIG = {
        "file": "fraktales_lernen_4"
    }
    def construct(self):
        svg = SVGMobject(self.file,stroke_width=2,fill_opacity=0)[1]
        svg.set_height(5)
        svg.append_points([svg.points[-1],svg.points[0]])
        self.add(svg)

class TestBranch(Scene):
    def construct(self):
        branch = self.get_branch(1/3.5,PI/5)
        self.add(branch)

    def get_branch(self,proportion=1/2,angle=PI/4):
        main_line = Line(DOWN,UP,color=YELLOW)
        up_line = Line(
            main_line.get_start(),
            main_line.point_from_proportion(proportion)
        )
        down_line = Line(
            main_line.point_from_proportion(proportion),
            main_line.get_end(),
        )
        branch = VGroup(
            main_line,
            down_line,
            up_line
        )
        branch.rotate(PI)
        left_line = up_line.copy()
        right_line = up_line.copy()
        left_line.rotate(angle,about_point=left_line.get_end())
        right_line.rotate(-angle,about_point=right_line.get_end())
        branch.add(left_line,right_line)
        branch.remove(main_line,up_line)
        return branch

class TextTest(Scene):
    def construct(self):
        text = TexMobject("\\mathbb{AM}",sheen_direction=DR)
        text.set_height(FRAME_HEIGHT-2)
        text.set_color(color=[BLUE,GREEN,YELLOW])
        self.add(text)

class TestTree(Scene):
    def construct(self):
        tree = SVGMobject("Green_v2")
        tree.set_width(FRAME_WIDTH-3)
        self.play(DrawBorderThenFill(tree))
        self.wait()

class BranchFractal(Scene):
    CONFIG = {
        "branch_config": {
            "angle": PI/6,
            "scale": 0.7
        }
    }
    def construct(self):
        main_branch = Line(DOWN,UP)
        main_branch.to_edge(DOWN)
        main_branch.shift(RIGHT*3)
        branch = VGroup(main_branch)
        self.branches_groups = VGroup()

        n2 = [2**n for n in range(10)]
        for n in n2:
            for mob in branch[-n:]:
                branch.add(*self.get_branches(mob,**self.branch_config))
        #branch.move_to(ORIGIN)
        #self.play(ShowCreation(branch))
        #self.add(branch)

        bg = self.branches_groups.copy()
        bg.set_color(RED)
        point = Dot().next_to(branch, LEFT, buff=3)
        list_group = self.get_list_group(4)
        copy = bg[0].copy()
        copy.move_to(point)
        self.play(ShowCreation(copy))

        for i in list_group:
            target = self.branches_groups[i].copy()
            target.set_color(RED)
            self.play(TransformFromCopy(copy,target))

        self.wait()

    def get_branches(self,main,angle,scale):
        def get_branch(mob,vector,scale):
            return Line(
                mob.get_end(),
                mob.get_end() + vector * mob.get_length() * scale,
            )
        unit_vector = main.get_unit_vector()
        first_branch_vector  = rotate_vector(unit_vector, angle)
        second_branch_vector = rotate_vector(unit_vector, -angle)
        first_branch = get_branch(main, first_branch_vector,scale)
        second_branch = get_branch(main, second_branch_vector,scale)
        self.branches_groups.add(
            VGroup(main,first_branch,second_branch)
        )
        return [first_branch, second_branch]

    def get_list_group(self, n):
        pre_list = [
            list( range(4**n - 1,2 * 4**n - 1 ) )
            for n in range(n)
        ]
        list_expanded = []
        for i in range(len(pre_list)):
            for j in pre_list[i]:
                list_expanded.append(j)
        return list_expanded

    def get_fractal(self, branch, level=10):
        branch.branches_group = VGroup()
        n2 = [2**n for n in range(10)]
        for n in n2:
            for mob in branch[-n:]:
                branch.add(*self.get_branches(mob,**self.branch_config))

class BranchTree(Scene):
    CONFIG = {
        "branch_config": {
            "angle": 35*DEGREES,
            "scale": 0.7
        }
    }
    def construct(self):
        tree = SVGMobject("Green_v2",color=RED)
        tree.set_width(FRAME_WIDTH-3)
        self.add(tree)
        fractal = VGroup(
            Line(DOWN,ORIGIN)
        )
        fractal_group = VGroup()
        self.get_fractal(fractal, fractal_group)
        fractal.set_height(tree.get_height()*0.9)
        fractal.next_to(tree.get_bottom(),UP,buff=0)
        self.add(fractal_group)

    def get_branches(self,main,group,angle,scale):
        def get_branch(mob,vector,scale):
            return Line(
                mob.get_end(),
                mob.get_end() + vector * mob.get_length() * scale,
            )
        unit_vector = main.get_unit_vector()
        first_branch_vector  = rotate_vector(unit_vector, angle)
        second_branch_vector = rotate_vector(unit_vector, -angle)
        first_branch = get_branch(main, first_branch_vector,scale)
        second_branch = get_branch(main, second_branch_vector,scale)
        group.add(
            VGroup(main,first_branch,second_branch)
        )
        return [first_branch, second_branch]

    def get_list_group(self, n):
        pre_list = [
            list(range(4**n - 1,2 * 4 ** n - 1))
            for n in range(n)
        ]
        list_expanded = []
        for i in range(len(pre_list)):
            for j in pre_list[i]:
                list_expanded.append(j)
        return list_expanded

    def get_fractal(self, branch, branch_group, level=10):
        n2 = [2**n for n in range(level)]
        for n in n2:
            for mob in branch[-n:]:
                branch.add(*self.get_branches(mob,branch_group,**self.branch_config))


class Test1(Scene):
    CONFIG = {
        "alphas": {
            "left": {
                "x": -0.77,
                "y": 0.01
            },
            "right": {
                "x": 0.4,
                "y": 0.5
            }
        }
    }
    def construct(self):
        svg = SVGMobject("head",stroke_width=0)
        svg.set_height(FRAME_HEIGHT - 4)
        colors = self.color_list()
        step = 0
        for mob in svg:
            try:
                mob.set_color(colors[step])
            except:
                break
            step += 1

        svg.to_edge(RIGHT)
        #---------------------------------------
        # self.beast_eyes.move_to(
        #     self.human_beast.get_center() + \
        #     self.human_beast_initial_height * self.beast_alpha["alpha_x"] * RIGHT + \
        #     self.human_beast_initial_height * self.beast_alpha["alpha_y"] * UP
        # )
        left_eye = Dot(radius=0.1)
        left_eye.move_to(
            svg.get_center() + \
            svg.get_height() * self.alphas["left"]["x"] * RIGHT + \
            svg.get_height() * self.alphas["left"]["y"] * UP
        )
        lights = Lights(left_eye,size=0.01,angle=40*DEGREES)
        self.add(svg)
        #self.play(GrowFromCenter(left_eye))
        self.add(lights)
        self.wait()
        self.play(
            UpdateFromAlphaFunc(lights,self.show_lights(lights,max_size=2))
        )
        self.wait()
    
    def get_numbers(self,svg,height=1,color=WHITE,direction=ORIGIN,buff=0.4):
        step = 0
        numbers = VGroup()
        for mob in svg:
            t = Text(f"{step}",font="Arial",stroke_width=0,height=height,color=color)
            if get_norm(direction) == 0:
                t.move_to(mob)
            else:
                t.next_to(mob,direction=direction,buff=buff)
            numbers.add(t)
            mob.append_points([mob.points[-1],mob.points[0]])
            step += 1
        return numbers

    def show_lights(self, lights, max_size):
        min_size = lights.size
        circle = Circle(radius=lights.radius)
        circle.move_to(lights.position)
        def update_lights(mob,alpha):
            size = interpolate(min_size,max_size,alpha)
            mob.become(
                Lights(circle,size=size,angle=lights.angle,color=lights.color)
            )
        return update_lights


    def color_list(self):
        import csv
        colors = []
        with open('projects/head_colors.csv', newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
            for row in spamreader:
                for color in row:
                    colors.append(color)
        return colors


class LightsCar(Scene):
    def construct(self):
        lights = Lights(size=0.3)
        self.add(lights)
        # circle = Circle(radius=0.5)
        # group = self.get_group(circle,20 * DEGREES, size=4)
        # lines, arc, arc2 = group
        # lines[1].rotate(PI)
        # polygon = Polygon(*lines[0].points,*lines[1].points,color=WHITE,fill_opacity=1)
        # total_group = VGroup(polygon, arc)
        # for mob in total_group:
        #     mob.set_stroke(width=0)
        # self.add(total_group)

    # def get_group(self,mob,operture_angle=0,size=1):
    #     theta_up   = 90  * DEGREES - operture_angle
    #     theta_down = 270 * DEGREES + operture_angle
    #     angles = [theta_up,theta_down]
    #     lines = VGroup()
    #     for theta in angles:
    #         point,angle,unit_vector = self.get_tangent_and_point(mob,theta)
    #         if theta > 270 * DEGREES:
    #             unit_vector = - unit_vector 
    #         line = Line(
    #             point,
    #             point + unit_vector * size
    #         )
    #         lines.add(line)

    #     arc = Arc(
    #         radius=mob.radius,
    #         start_angle=theta_up,
    #         angle=(theta_down-theta_up),
    #         fill_opacity=1,
    #         fill_color=BLACK,
    #     )

    #     inter,distance = self.get_big_circle(lines)
    #     arc2 = Arc(
    #         radius=distance,
    #         start_angle=theta_up,
    #         angle=(theta_down-theta_up),
    #         arc_center=inter,
    #         fill_opacity=1,
    #     )
    #     group = VGroup(lines, arc, arc2)
    #     return group

    # def get_tangent_and_point(self,mob, angle):
    #     point = mob.point_at_angle(angle)
    #     radius = Line(
    #         mob.get_center(),
    #         point
    #     )
    #     radius.move_to(point)
    #     radius.rotate(PI/2)
    #     angle = radius.get_angle()
    #     unit_vector = radius.get_unit_vector()
    #     return point,angle,unit_vector

    # def get_big_circle(self,lines):
    #     coords = []
    #     for _line in lines:
    #         line = _line.copy()
    #         line.move_to(_line.get_end())
    #         line.rotate(PI/2)
    #         x_coord = line.get_start()
    #         y_coord = line.get_end()
    #         coords.append([x_coord,y_coord])

    #     intersection = line_intersection(coords[0],coords[1])
    #     distance = get_norm(lines[0].get_end() - intersection)

    #     return intersection,distance