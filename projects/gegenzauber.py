from manimlib.imports import *
from io import *

def get_sign_from_even(n):
    return 1 if n % 2 == 0 else -1

def get_dim_from_vector(vector):
    for i, dim in enumerate(vector):
        if abs(dim) == 1:
            return i

def Range(start,end=None):
    if end == None:
        return range(start+1)
    return range(start,end+1)

# BLACK = WHITE
# WHITE = "#000000"

class SubTexMobject(TexMobject):
    CONFIG = {
        "background_stroke_width": 0.2,
    }


class TexMobject(TexMobject):
    CONFIG = {
        "background_stroke_width": 0.2,
        "color": BLACK
    }




FILE = "gegenzauber"
TEX_CLASS = TexMobject

formula_file = open(f"FORMULAS/TXT/{FILE}.txt","r")
formula_file = formula_file.readlines()

MAGIC_COLOR = RED
ANTI_MAGIC_COLOR = YELLOW

TABLE_COLORS = {
    "plus_minus": BLUE_D,
    "by_division": "#9336ff", 
    "radical": "#ff7929",
    "trigonometric": "#568625",
    "log": "#0532fe"
}

PROCESS_COLORS = [
    "by_division",
    "log",
    "plus_minus",
    "by_division",
    "trigonometric",
    "plus_minus",
    "by_division",
]

# self.remark_part_formula(0,[*range(22,31)],r"\frac{(\phantom{a})}{\sin\left(\frac{\pi}{2}\right)}",r"(\phantom{a})\times\sin\left(\frac{\pi}{2}\right)")
# remark_part_formula(self,n_step,rang,mg_tex,amg_tex,type_op="by_division"):
N_STEPS = [even for even in [*range(15)]]

FORMULAS_REPLACE = {
    1: r"\frac{(\phantom{a})}{\sin\left(\frac{\pi}{2}\right)}",
    2: r"(\phantom{a})\times\sin\left(\frac{\pi}{2}\right)",
    3: r"e^{(\phantom{a})}",
    4: r"\ln(\phantom{a})",
    5: r"-1",
    6: r"+1",
    7: r"\frac{(\phantom{a})}{\cos(12)}",
    8: r"(\phantom{a})\times\cos(12)",
    9: r"\sin(\phantom{a})",
    10: r"\arcsin(\phantom{a})",
    11: r"-5\pi",
    12: r"+5\pi",
    13: r"\times 12",
    14: r"\frac{(\phantom{a})}{12}",
}

PLUS_MINUS_RANGE = {
    3: [19,20],
    4: [33+1,34+1],
    5: [26+1,27+1],
    6: [3,4,5,28+1,29+1],
    7: [25+1,26+1,36+1+1,37+1+1,38+1+1],
    8: [23+1,24+1,34+1+1,35+1+1,36+1+1]
}

BY_DIVISION_RANGE = {
    1: [*Range(22,30)],
    2: [*Range(24,32)],
    3: [*Range(26,33+1)],
    4: [*Range(11,18),*Range(24,31+1)],
    5: [*Range(17,24+1),*Range(29+1,35+1+1)],
    6: [*Range(19,26+1),*Range(31+1,37+1+1)],
    7: [0,1,*Range(16,23+1),*Range(28+1,34+1+1)],
    8: [*Range(14,21+1),*Range(26+1,32+1+1),*Range(37+1+1,39+1+1)]
}

RADICAL_RANGE = {

}

TRIGONOMETRIC_RANGE = {
    5: [0,1,2,3,10],
    6: [*Range(7,13),38+1+1],
    7: [*Range(4,10),35+1+1],
    8: [*Range(2,8),33+1+1],
}

LOG_RANGE = {
    2: [0],
    3: [*Range(22,24),34+1],
    4: [*Range(20,22),32+1],
    5: [*Range(13,15),25+1],
    6: [*Range(15,17),27+1],
    7: [*Range(12,14),24+1],
    8: [*Range(10,12),22+1],
}

EQUAL_INDEX = [
    31,
    22,
    21,
    19,
    11,
    6,
    3,
    1
]

RIGHT_COLUMN_FORMULAS_INDEX = [
    [*Range(24,32)],
    [22,23,24,35],
    [34,35],
    [*Range(30,37)],
    [*Range(7,13),40],
    [37+1,38+1,39+1],
    [38+1,39+1,40+1]
]

LEFT_ALIGNED_EDGES = [
    23,25,20,13,14
][::-1]

X_RED_INDEXES = [
    7,7,6,6,6,2,2,0
]

class RectangleTex(VGroup):
    CONFIG = {
        "rectangle_config": {
            "stroke_color": BLACK,
            "width": 2.2,
            "height": FRAME_HEIGHT/9,
            "stroke_opacity": 1,
            "fill_opacity": 0,
            "stroke_width": 1,
            "fill_color": BLACK
        },
        "tex_config": {
        },
        "tex_rectangle_proportion": 0.5
    }
    def __init__(self,*tex,**kwargs):
        super().__init__(**kwargs)
        self.tex = tex = SubTexMobject(*tex,**self.tex_config)
        self.rectangle = rectangle = Rectangle(**self.rectangle_config)
        tex.move_to(rectangle)
        self.add(
            rectangle,
            tex
        )

    def get_rectangle(self):
        return self.rectangle

    def get_tex(self):
        return self.tex


class Gegenzauber(VGroup):
    CONFIG = {
        "colors": TABLE_COLORS,
        "tex_config": {},
        "rectangle_config": {
            "fill_opacity": 1,
            "fill_color": BLACK
        }
    }
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        pairs_tex = [
            [
                [r"(\phantom{a})+",      r"(\phantom{a})-"],
            ],
            [
                [r"(\phantom{a})\times", r"(\phantom{a}):"],
            ],
            [
                [r"(\phantom{a})^{2}",  r"\sqrt{(\phantom{a})}"],
                [r"(\phantom{a})^{3}",r"\sqrt[3]{(\phantom{a})}"],
            ],
            [
                [r"\sin(\phantom{a})",   r"\arcsin(\phantom{a})"],
                [r"\cos(\phantom{a})",   r"\arccos(\phantom{a})"],
                [r"\tan(\phantom{a})",   r"\arctan(\phantom{a})"],
            ],
            [
                [r"a^{(\phantom{a})}",   r"\log_a(\phantom{a})"],
                [r"e^{(\phantom{a})}",   r"\ln(\phantom{a})"],
            ]
        ]
        grp = VGroup(*[
            VGroup(*[
                VGroup(*[
                    RectangleTex(
                        subsubmob,
                        tex_config={"color":color,"background_stroke_color": color})
                    for subsubmob in submob
                ]).arrange(RIGHT,buff=0)
                for submob in mob
            ]).arrange(DOWN,buff=0)
            for color,mob in zip(self.colors.values(),pairs_tex)
        ]).arrange(DOWN,buff=0)
        self.plus_minus    = grp[0][0]
        self.by_division   = grp[1][0]
        self.even_root     = grp[2][0]
        self.odd_root      = grp[2][1]
        self.trig_sin      = grp[3][0]
        self.trig_cos      = grp[3][1]
        self.trig_tan      = grp[3][2]
        self.power         = grp[4][0]
        self.e_power       = grp[4][1]
        self.add(*grp)

class FadeTransform(Transform):
    CONFIG = {
        "stretch": True,
        "dim_to_match": 1,
    }

    def __init__(self, mobject, target_mobject, **kwargs):
        self.to_add_on_completion = target_mobject
        mobject.save_state()
        super().__init__(
            Group(mobject, target_mobject.copy()),
            **kwargs
        )

    def begin(self):
        self.ending_mobject = self.mobject.copy()
        Animation.begin(self)
        # Both 'start' and 'end' consists of the source and target mobjects.
        # At the start, the traget should be faded replacing the source,
        # and at the end it should be the other way around.
        start, end = self.starting_mobject, self.ending_mobject
        for m0, m1 in ((start[1], start[0]), (end[0], end[1])):
            self.ghost_to(m0, m1)

    def ghost_to(self, source, target):
        source.replace(target, stretch=self.stretch, dim_to_match=self.dim_to_match)
        source.set_opacity(0)

    def get_all_mobjects(self):
        return [
            self.mobject,
            self.starting_mobject,
            self.ending_mobject,
        ]

    def get_all_families_zipped(self):
        return Animation.get_all_families_zipped(self)

    def clean_up_from_scene(self, scene):
        Animation.clean_up_from_scene(self, scene)
        scene.remove(self.mobject)
        self.mobject[0].restore()
        scene.add(self.to_add_on_completion)


class FadeTransformPieces(FadeTransform):
    def begin(self):
        self.mobject[0].align_family(self.mobject[1])
        super().begin()

    def ghost_to(self, source, target):
        for sm0, sm1 in zip(source.get_family(), target.get_family()):
            super().ghost_to(sm0, sm1)

class MainScene(MovingCameraScene):
    CONFIG = {
        "run_time_list": [
            2.3 for _ in range(7)
        ],
        "camera_config": {
            "background_color": WHITE
        }
    }
    def setup(self):
        MovingCameraScene.setup(self)
        # grp setup
        self.grp = grp = Gegenzauber()
        # grp.set_height(FRAME_HEIGHT-1)
        # grp.scale(0.9)
        grp.scale(0.44)
        # formulas and rect_formula setup
        grp.align_to(self.camera_frame,DOWN)
        grp.align_to(self.camera_frame,RIGHT)
        grp.shift(UP*0.01)
        # add to screen
        self.setup_formulas()
        self.formulas.scale(0.7)
        self.formulas.arrange(DOWN,aligned_edge=LEFT,buff=0.3)
        self.formulas.align_to(self.camera_frame,LEFT)
        self.formulas.align_to(self.camera_frame,UP)
        self.formulas.shift(DR*0.3)
        #self.formulas[:4].scale(1.4)
        # Arrange about =
        x_max_coord_right_member = max(*[
            self.formulas[i][j].get_x()
            for i,j in zip(range(len(self.formulas)),EQUAL_INDEX)
        ])
        x_max_coord_left_member = max(*[
            self.formulas[i][:j].get_x()
            for i,j in zip(range(len(self.formulas)),EQUAL_INDEX)
        ])
        n_t = VGroup(*[
            Text(f"{i+1}",font="Arial",height=0.7,stroke_width=0).next_to(self.formulas[i],LEFT)
            for i in range(len(self.formulas))
        ])
        max_height_formula = max(*[
            mob.get_height() for mob in self.formulas
        ])
        for mob,j in zip(self.formulas,EQUAL_INDEX):
            mob[j].set_x(x_max_coord_right_member)
            prev_y = mob[j+1:].get_y()
            mob[j+1:].next_to(mob[j],RIGHT,buff=0.2)
            mob[j+1:].set_y(prev_y)
            mob[:j].set_x(x_max_coord_left_member)
        len_lae = len(LEFT_ALIGNED_EDGES)
        for i,j,w in zip(range(len_lae),LEFT_ALIGNED_EDGES,EQUAL_INDEX[:len_lae][::-1]):
            reference_eq = self.formulas[len_lae-i][j]
            eq_to_move = self.formulas[len_lae-i-1][w+1:]
            eq_to_move.align_to(reference_eq,LEFT)
        right_vertical_lines = VGroup(*[
            Line(ORIGIN,DOWN,color=BLACK)
                .set_height(max_height_formula*0.8)
                .next_to(self.formulas,RIGHT,buff=0.6)
                .set_y(mob.get_y())
            for mob in self.formulas
        ])
        right_vertical_lines[-1].fade(1)
        right_column_formulas = VGroup(*[
            self.get_pre_formula_right_column(i)
                .next_to(right_vertical_lines[i])
            for i in range(len(self.formulas)-1)
        ])
        self.right_vertical_lines = right_vertical_lines
        self.right_column_formulas = right_column_formulas
        # self.add(
        #     grp,
        #     self.formulas,
        #     right_column_formulas,
        #     right_vertical_lines
        # )


    def construct(self):
        grp, formulas, = self.grp, self.formulas
        rvl = self.right_vertical_lines
        rcf = self.right_column_formulas
        for formula in self.formulas:
            formula.save_state()
            formula.set_color(BLACK)
        formulas[4][:11].set_color(BLACK)
        for i,f in enumerate(self.formulas):
            f.saved_state[X_RED_INDEXES[i]].set_color(RED)
            f[X_RED_INDEXES[i]].set_color(RED)
        # self.add(
        #     grp,
        #     formulas[0],
        # )
        self.play(
            Write(grp),
            Write(formulas[0])
        )
        self.wait()
        # STEP 1 \times\sin(pi/2)----------------------------------------------
        self.select_and_invert(0,[22,30],side=LEFT,attr="by_division",run_time=self.run_time_list[0])
        left_copy = formulas[0][22:31].copy()
        self.play(
            *[
                TransformFromCopy(
                    formulas[0][i],
                    formulas[1][j],
                )
                for i,j in zip([*Range(0,21),31,32],[*Range(21),22,23])
            ],
            left_copy.next_to,formulas[1][:22],DOWN,{"buff":0.1},
            run_time=3
        )
        self.play(
            TransformFromCopy(
                rcf[0][:],
                self.formulas[1][24:].set_color(rcf[0][0].get_color())
            ),
            FadeOut(left_copy),
            run_time=self.run_time_list[0]
        )
        # STEP 2 \ln()----------------------------------------------
        self.select_and_invert(1,[0,1],side=RIGHT,attr="e_power",run_time=self.run_time_list[1])
        self.formulas[2][26:35].set_color(TABLE_COLORS["by_division"])
        left_copy = self.formulas[1][:22].copy()
        self.play(
            left_copy.set_y,self.formulas[2][21].get_y(),
            TransformFromCopy(
                self.formulas[1][22],
                self.formulas[2][21],
            ),
            run_time=self.run_time_list[1]
        )
        self.play(
            *[
                FadeTransformPieces(
                    pre,
                    pos
                )
                for pre,pos in zip(left_copy[1:],self.formulas[2][:21])
            ],
            FadeOut(left_copy[0]),
            TransformFromCopy(
                rcf[1][:],
                VGroup(*[
                    formulas[2][i] 
                    for i in [22,23,24,35]
                ]).set_color(rcf[1][0].get_color())
            ),
            run_time=self.run_time_list[1]
        )
        self.step_formula(2,changes=[[
            [*Range(23,32)],
            [*Range(25,34)]
            ]],
            run_time=self.run_time_list[1],
        )
        # print("------------")
        # Step 3: +1 ------------------------------------------------
        print("step 3")
        self.select_and_invert(2,[19,21],side=LEFT,attr="plus_minus",run_time=self.run_time_list[2])
        formulas[3].restore()
        formulas[3].save_state()
        formulas[3][11:19].set_color(BLACK)
        left_copy = formulas[2][:21].copy()
        self.play(
            *[
                TransformFromCopy(
                    formulas[2][i],
                    formulas[3][j],
                )
                for i,j in zip([*Range(21,35)],[*Range(19,35)])
            ],
            left_copy.set_y,formulas[3][19].get_y(),
            run_time=3
        )
        self.play(
            TransformFromCopy(
                rcf[2][:],
                self.formulas[3][34:].set_color(TABLE_COLORS["plus_minus"])
            ),
            ReplacementTransform(
                left_copy[:19],
                formulas[3][:19].set_x(left_copy[:19].get_x()),
            ),
            left_copy[19:].fade,1,
            # FadeOut(left_copy),
            run_time=self.run_time_list[2]
        )
        # Step 4: \cos(12) ------------------------------------------
        print("step 4")
        formulas[3].saved_state[:19].set_x(formulas[3][:19].get_x())
        self.select_and_invert(3,[11,19],side=LEFT,attr="by_division",run_time=self.run_time_list[3])
        left_copy = formulas[3][11:19].copy()
        formulas[4].restore()
        formulas[4].save_state()
        formulas[4][:11].set_color(BLACK)
        formulas[4][6].set_color(RED)
        self.play(
            *[
                TransformFromCopy(
                    formulas[3][i],
                    formulas[4][j].set_x(formulas[3][i].get_x()),
                )
                for i,j in zip([*Range(10),*Range(19,35)],[*Range(10),11,*Range(13,28)])
            ],
            left_copy.next_to,formulas[4][:11],DOWN,{"buff":0.1},
            run_time=self.run_time_list[3]
        )
        self.play(
            TransformFromCopy(
                rcf[3][:],
                self.formulas[4][30:].set_color(rcf[3][0].get_color())
            ),
            *[GrowFromCenter(formulas[4][i]) for i in [12,29]],
            left_copy.fade,1,
            # FadeOut(left_copy),
            run_time=self.run_time_list[3]
        )
        # Step 5: \arcsin() -----------------------------------------
        print("Step 5")
        formulas[4].saved_state[:11].set_x(formulas[4][:11].get_x())
        self.select_and_invert(4,[0,3],side=RIGHT,attr="trig_sin",run_time=self.run_time_list[4])
        # formulas[2][26:35].set_color(TABLE_COLORS["by_division"])
        left_copy = self.formulas[4][:11].copy()
        formulas[5].restore()
        formulas[5].save_state()
        formulas[5][3:6].set_color(BLACK)
        self.play(
            left_copy.set_y,self.formulas[5][6].get_y(),
            TransformFromCopy(
                self.formulas[4][11],
                self.formulas[5][6],
            ),
            run_time=self.run_time_list[4]
        )
        self.play(
            *[
                ReplacementTransform(
                    pre,
                    pos.set_x(pre.get_x())
                )
                for pre,pos in zip(left_copy[4:10],formulas[5][:6])
            ],
            FadeOut(left_copy[0:4]),
            FadeOut(left_copy[-1]),
            TransformFromCopy(
                rcf[4][:],
                VGroup(*[
                    formulas[5][i] 
                    for i in [*Range(7,13),39+1]
                ]).set_color(rcf[4][0].get_color())
            ),
            run_time=self.run_time_list[4]
        )
        self.step_formula(5,changes=[[
            [*Range(12,37)],
            [*Range(14,39)]
            ]],
            run_time=self.run_time_list[4]
        )
        # Step 6: +5\pi ---------------------------------------------
        print("Step 6")
        formulas[5].saved_state[:6].set_x(formulas[5][:6].get_x())
        self.select_and_invert(5,[3,6],side=LEFT,attr="plus_minus",run_time=self.run_time_list[5])
        formulas[6].restore()
        formulas[6].save_state()
        formulas[6][:2].set_color(BLACK)
        left_copy = formulas[5][:6].copy()
        self.play(
            *[
                TransformFromCopy(
                    formulas[5][i],
                    formulas[6][j],
                )
                for i,j in zip([*Range(6,39+1)],[*Range(3,36+1)])
            ],
            left_copy.set_y,formulas[6][3].get_y(),
            run_time=self.run_time_list[5]
        )
        self.play(
            TransformFromCopy(
                rcf[5][:],
                self.formulas[6][37+1:].set_color(rcf[5][0].get_color())
            ),
            ReplacementTransform(
                left_copy[:3],
                formulas[6][:3].set_x(left_copy[:3].get_x()),
            ),
            left_copy[3:].fade,1,
            # FadeOut(left_copy),
            run_time=self.run_time_list[2]
        )
        # Step 7: /12 -----------------------------------------------
        print("Step 7")
        formulas[6].saved_state[:3].set_x(formulas[6][:3].get_x())
        self.select_and_invert(6,[0,2],side=RIGHT,attr="by_division",run_time=self.run_time_list[6])
        left_copy = formulas[6][:3].copy()
        formulas[7].restore()
        self.play(
            *[
                TransformFromCopy(
                    formulas[6][i],
                    formulas[7][j].set_x(formulas[6][i].get_x()),
                )
                for i,j in zip([*Range(3,39+1)],[*Range(1,37+1)])
            ],
            left_copy.set_y,formulas[7][1].get_y(),
            run_time=self.run_time_list[6]
        )
        formulas[7][38+1:].set_width(formulas[7][2:38+1].get_width())
        formulas[7][38+1:].set_x(formulas[7][2:38+1].get_x())
        self.play(
            TransformFromCopy(
                rcf[6][:],
                formulas[7][38+1:].set_color(rcf[6][0].get_color())
            ),
            left_copy[:2].fade,1,
            # FadeOut(left_copy),
            run_time=self.run_time_list[6]
        )
        self.wait(2)

    def get_pre_formula_right_column(self,index):
        formula = VGroup(*[
            self.formulas[index+1][i]
            for i in RIGHT_COLUMN_FORMULAS_INDEX[index]
        ]).copy()
        if index in [1,4]:
            formula[-1].align_to(formula[-2],RIGHT)
            formula[-1].shift(formula[-1].get_width()*RIGHT*1.4)
        if index == 6:
            formula[0].set_width(formula[1:].get_width()*1.2)
            formula[0][0].set_stroke(width=3)
            formula[0].next_to(formula[1:],UP,buff=0.1)
        return formula

    def select_and_invert(self,i,rang,side=RIGHT,attr="plus_minus",run_time=2):
        index = 0 if side[0] > 0 else 1
        op_type = getattr(self.grp,attr)
        op_type_side = op_type[index].rectangle.copy()
        op_type_side.set_style(stroke_opacity=0)
        op_type_side.scale(0.97)
        op_type_inverse_side = op_type[(index+1)%2].rectangle
        rvl = self.right_vertical_lines
        rcf = self.right_column_formulas
        if abs(rang[0]-rang[1]) == 1:
            formula_rectangle = Rectangle(
                width=self.formulas[i][rang[0]].get_width()+0.15,
                height=self.formulas[i][rang[0]].get_height()+0.15,
                color=YELLOW,
                stroke_width=2
            ).move_to(self.formulas[i][rang[0]])
        else:
            formula_rectangle = Rectangle(
                width=self.formulas[i][rang[0]:rang[1]].get_width()+0.15,
                height=self.formulas[i][rang[0]:rang[1]].get_height()+0.15,
                color=YELLOW,
                stroke_width=2
            ).move_to(self.formulas[i][rang[0]:rang[1]])
        table_rectangle = Rectangle(
            width=op_type_side.get_width(),
            height=op_type_side.get_height(),
            color=YELLOW,
            stroke_width=2
        ).move_to(op_type_side)
        self.bring_to_front(op_type_side)
        self.bring_to_front(op_type[index].tex)
        self.play(
            Restore(self.formulas[i]),
            ShowCreation(formula_rectangle),
            # FocusOn(formula_rectangle),
            run_time=run_time
        )
        self.wait()
        self.play(
            # ShowCreation(table_rectangle),
            TransformFromCopy(formula_rectangle,table_rectangle),
            op_type_side.set_style, {"fill_color": RED,"fill_opacity": 0.5},
            run_time=run_time
        )
        self.play(
            table_rectangle.move_to,op_type_inverse_side,
            op_type_side.move_to,op_type_inverse_side,
            op_type_side.set_color, GREEN,
            run_time=1.6
        )
        self.play(
            TransformFromCopy(
                op_type[(index+1)%2].tex,
                rcf[i][:],
                run_time=run_time+1
            ),
            GrowFromCenter(rvl[i]),
            run_time=run_time+1
        )
        self.play(
            op_type_side.fade,1,
            FadeOut(table_rectangle),
            FadeOut(formula_rectangle),
            run_time=2.5
        )

    def setup_formulas(self):
        self.formulas = VGroup(*[
            TexMobject(f)[0] for f in formula_file
        ])
        self.formulas.scale(0.9)
        for i in LOG_RANGE.keys():
            for w in LOG_RANGE[i]:
                # print(f"i: {i}, w: {w}")
                self.formulas[i-1][w].set_color(TABLE_COLORS["log"])
                self.formulas[i-1][w].background_stroke_color = TABLE_COLORS["log"]
        for i in TRIGONOMETRIC_RANGE.keys():
            for w in TRIGONOMETRIC_RANGE[i]:
                # print(f"i: {i}, w: {w}")
                self.formulas[i-1][w].set_color(TABLE_COLORS["trigonometric"])
                self.formulas[i-1][w].background_stroke_color = TABLE_COLORS["trigonometric"]
        for i in BY_DIVISION_RANGE.keys():
            for w in BY_DIVISION_RANGE[i]:
                # print(f"i: {i}, w: {w}")
                self.formulas[i-1][w].set_color(TABLE_COLORS["by_division"])
                self.formulas[i-1][w].background_stroke_color = TABLE_COLORS["by_division"]
        for i in PLUS_MINUS_RANGE.keys():
            for w in PLUS_MINUS_RANGE[i]:
                # print(f"i: {i}, w: {w}")
                self.formulas[i-1][w].set_color(TABLE_COLORS["plus_minus"])
                self.formulas[i-1][w].background_stroke_color = TABLE_COLORS["plus_minus"]

    def get_magic_rectangle(self,grp,big_rectangle):
        mg = Rectangle(
            width=big_rectangle.get_width()*0.96/2,
            height=grp.get_height()*3*0.9/9
        )
        mg.align_to(grp,DOWN)
        return mg

    def step_formula(self,
                            n_step=0,
                            changes=[[]],
                            pos_write=[],
                            pre_fade=[],
                            fade=[],
                            write=[],
                            pre_copy=[],
                            pos_copy=[],
                            time_pre_changes=0.3,
                            time_pos_changes=0.3,
                            run_time=2,
                            time_end=0.3,
                            transform_class=TransformFromCopy
                            ):
        print(f"step: {n_step}")
        formula_copy=[]

        pre_formula = self.formulas[n_step - 1]
        pos_formula = self.formulas[n_step]

        for c in pre_copy:
            formula_copy.append(pre_formula[c].copy())

        if len(pre_fade) > 0:
            self.play(*[
                FadeOut(pre_formula[w])
                for w in pre_fade
            ])

        self.wait(time_pre_changes)

        for pre_ind,post_ind in changes:
            self.play(
                *[
                    transform_class(
                        pre_formula[i],pos_formula[j],
                    )
                    for i,j in zip(pre_ind,post_ind)
                ],
                *[
                    FadeOut(pre_formula[f])
                    for f in fade
                ],
                *[
                    Write(pos_formula[w])
                    for w in write
                ],
                *[
                    transform_class(
                        formula_copy[j],
                        pos_formula[f]
                    )
                    for j,f in zip(
                                    range(len(pos_copy)),
                                    pos_copy
                                )
                ],
                run_time=run_time
            )

        self.wait(time_pos_changes)
        
        if len(pos_write) > 0:
            self.play(*[
                Write(pos_formula[w])
                for w in pos_write
            ])

        self.wait(time_end)

    def fit_mob_to_another(self,mob,rect):
        mob_ratio = mob.get_width() / mob.get_height()
        rect_ratio = rect.get_width() / rect.get_height()
        if mob_ratio >= rect_ratio:
            mob.set_width(rect.get_width()*0.8)
        else:
            mob.set_height(rect.get_height()*0.8)

# print(N_STEPS)
# RANGES_MAGIC_COLORS = [
#     [*Range(22,30)],
#     [*Range(22,30)],
#     [0],
#     [3],
#     [19,20],
#     [19,20],
#     [*Range(11,18)],
#     [*Range(11,18)],
#     [*Range(0,3),10],
#     [*Range(7,10),17],
#     [3,4,5],
#     [3,4,5],
#     [0,1],
#     [0,1],
#     []
# ]
# RANGES_ANTI_MAGIC_COLORS = [
#     [],
#     [*Range(31,39),*Range(42,50)],
#     [],
#     [0,1,2,25,27,28,29,39],
#     [],
#     [21,22,37,38],
#     [],
#     [*Range(19,26),*Range(45,52)],
#     [],
#     [*Range(6),18,*Range(20,26),51],
#     [],
#     [6,7,8,42,43,44],
#     [],
#     [3,4,5,42,43,44],
#     []
# ]
    # [*Range(31,29),*Range(42,50)],
    # [0],
    # [0,1,2,25,27,28,29,39],
    # [19,20],
    # []
