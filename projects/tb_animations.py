from manimlib.imports import *

def return_random_from_word(word):
    """
    This function receives a TextMobject, 
    obtains its length: 
        len(TextMobject("Some text"))
    and returns a random list, example:

    INPUT: word = TextMobjecT("Hello")
    length = len(word) # 4
    rango = list(range(length)) # [0,1,2,3]

    OUTPUT: [3,0,2,1] # Random list
    """
    rango = list(range(len(word)))
    random.shuffle(rango)
    return rango

def return_random_direction(word):
    """
    This function returns a list of random UP or DOWN:
    [UP,UP,DOWN,UP,DOWN,DOWN,...]
    """
    return [random.choice([UP,DOWN]) for _ in range(len(word))]

def get_random_coord(r_x,r_y,step_x,step_y):
    """
    Given two ranges (a, b) and (c, d), this function returns an 
    intermediate array (x, y) such that "x" belongs to (a, c) 
    and "y" belongs to (b, d).
    """
    range_x = list(range(r_x[0],r_x[1],step_x))
    range_y = list(range(r_y[0],r_y[1],step_y))
    select_x = random.choice(range_x)
    select_y = random.choice(range_y)
    return np.array([select_x,select_y,0])

def return_random_coords(word,r_x,r_y,step_x,step_y):
    """
    This function returns a random coordinate array, 
    given the length of a TextMobject
    """
    rango = range(len(word))
    return [word.get_center() + get_random_coord(r_x,r_y,step_x,step_y) for _ in rango]


class WriteRandom(LaggedStart):
    CONFIG = {
        "lag_ratio":0.1,
        "run_time":2.5,
        "anim_kwargs":{},
        "anim_type":Write
    }
    def __init__(self,text,**kwargs):
        digest_config(self, kwargs)
        super().__init__(*[
            self.anim_type(text[i],**self.anim_kwargs)
            for i in return_random_from_word(text)
        ])

class UnWriteRandom(WriteRandom):
    CONFIG = {
        "anim_kwargs": {
            "rate_func": lambda t: smooth(1-t)
        },
        "remover": True,
    }

class FadeInRandom(WriteRandom):
    CONFIG = {
        "anim_type": FadeIn
    }

class FadeInFromLargeRandom(WriteRandom):
    CONFIG = {
        "anim_type": FadeInFromLarge
    }

class FadeOutRandom(WriteRandom):
    CONFIG = {
        "anim_type": FadeOut
    }

class GrowRandom(WriteRandom):
    CONFIG = {
        "anim_type": GrowFromCenter
    }

class UnGrowRandom(GrowRandom):
    CONFIG = {
        "anim_kwargs": {
            "rate_func": lambda t: smooth(1-t),
        },
        "remover": True,
    }

class FadeInFromRandom(LaggedStart):
    CONFIG = {
        "lag_ratio":0.08,
        "anim_type":FadeInFrom,
        "anim_kwargs":{}
    }
    def __init__(self,text,**kwargs):
        digest_config(self, kwargs)
        super().__init__(*[
            self.anim_type(text[i],d,**self.anim_kwargs)
            for i,d in zip(return_random_from_word(text),return_random_direction(text))
        ])

class FadeOutFromRandom(FadeInFromRandom):
    CONFIG = {
        "anim_type":FadeOutAndShiftDown
    }

class GrowFromRandom(LaggedStart):
    CONFIG = {
        "lag_ratio":0.2,
        "anim_kwargs":{}
    }
    def __init__(self,text,r_x=[-2,3],r_y=[-2,3],step_x=1,step_y=1,**kwargs):
        digest_config(self, kwargs)
        super().__init__(*[
            GrowFromPoint(text[i],d,**self.anim_kwargs)
            for i,d in zip(return_random_from_word(text),return_random_coords(text,r_x,r_y,step_x,step_y))
        ])

class UnGrowFromRandom(GrowFromRandom):
    CONFIG = {
        "anim_kwargs": {
            "rate_func": lambda t: smooth(1-t)
        },
        "remover": True
    }

class WriteRandomScene(Scene):
    def construct(self):
        text = TextMobject("This is some text").set_width(FRAME_WIDTH-0.5)
        self.wait(3)
        # Why text[0]?
        # answer: https://www.youtube.com/watch?v=qfifBmYTEfA
        self.play(WriteRandom(text[0]))
        self.wait()
        self.play(UnWriteRandom(text[0]))
        self.wait(3)


class FadeFromRandomScene(Scene):
    def construct(self):
        text = TextMobject("This is some text").set_width(FRAME_WIDTH-0.5)
        # Why text[0]?
        # answer: https://www.youtube.com/watch?v=qfifBmYTEfA
        self.play(FadeInFromRandom(text[0]))
        self.wait()
        self.play(FadeOutFromRandom(text[0]))
        self.wait(3)

class GrowFromRandomScene(Scene):
    def construct(self):
        text = TextMobject("This is some text").set_width(FRAME_WIDTH-0.5)
        # Why text[0]?
        # answer: https://www.youtube.com/watch?v=qfifBmYTEfA
        self.play(GrowFromRandom(text[0]))
        self.wait()
        self.play(UnGrowFromRandom(text[0]))
        self.wait(3)

class FadeRandomScene(Scene):
    def construct(self):
        text = TextMobject("This is some text").set_width(FRAME_WIDTH-0.5)
        # Why text[0]?
        # answer: https://www.youtube.com/watch?v=qfifBmYTEfA
        self.play(FadeInRandom(text[0]))
        self.wait()
        self.play(FadeOutRandom(text[0]))
        self.wait(3)

class GrowRandomScene(Scene):
    def construct(self):
        text = TextMobject("This is some text").set_width(FRAME_WIDTH-0.5)
        # Why text[0]?
        # answer: https://www.youtube.com/watch?v=qfifBmYTEfA
        self.play(GrowRandom(text[0]))
        self.wait()
        self.play(UnGrowRandom(text[0]))
        self.wait(3)


class Lights(VGroup):
    def __init__(self, circle,color=WHITE ,angle=10 * DEGREES,size=1,flip=False,**kwargs):
        digest_config(self,kwargs)
        super().__init__()
        group = self.get_group(circle,angle, size=size)
        lines, arc, arc2 = group
        lines[1].rotate(PI)
        polygon = Polygon(*lines[0].points,*lines[1].points,color=color,sheen_direction=RIGHT,fill_opacity=1)
        VGroup(polygon,arc).set_stroke(width=0)
        polygon.set_color(color=[BLACK,color])
        self.add(polygon)
        if flip:
            self.rotate(PI,about_point=self.get_right(),axis=UP)
            polygon.set_color(color=[color,BLACK])
        self.radius = circle.radius
        self.size = size
        self.position = circle.get_center()
        self.angle = angle
        self.color = color
        self._flip = flip

    def get_group(self,mob,operture_angle=0,size=1):
        theta_up   = 90  * DEGREES - operture_angle
        theta_down = 270 * DEGREES + operture_angle
        angles = [theta_up,theta_down]
        lines = VGroup()
        for theta in angles:
            point,angle,unit_vector = self.get_tangent_and_point(mob,theta)
            if theta > 270 * DEGREES:
                unit_vector = - unit_vector 
            line = Line(
                point,
                point + unit_vector * size
            )
            lines.add(line)

        arc = Arc(
            radius=mob.radius,
            start_angle=theta_up,
            angle=(theta_down-theta_up),
            fill_opacity=1,
            fill_color=BLACK,
        )

        inter,distance = self.get_big_circle(lines)
        arc2 = Arc(
            radius=distance,
            start_angle=theta_up,
            angle=(theta_down-theta_up),
            arc_center=inter,
            fill_opacity=1,
        )
        group = VGroup(lines, arc, arc2)
        return group

    def get_tangent_and_point(self,mob, angle):
        point = mob.point_at_angle(angle)
        radius = Line(
            mob.get_center(),
            point
        )
        radius.move_to(point)
        radius.rotate(PI/2)
        angle = radius.get_angle()
        unit_vector = radius.get_unit_vector()
        return point,angle,unit_vector

    def get_big_circle(self,lines):
        coords = []
        for _line in lines:
            line = _line.copy()
            line.move_to(_line.get_end())
            line.rotate(PI/2)
            x_coord = line.get_start()
            y_coord = line.get_end()
            coords.append([x_coord,y_coord])

        intersection = line_intersection(coords[0],coords[1])
        distance = get_norm(lines[0].get_end() - intersection)

        return intersection,distance